<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:54 PM
 */

namespace app\models;


class PolicyKpi extends \app\models\base\PolicyKpi
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['policy_id'] = "Policy";
        return $labels;

    }

    public function getName(){
        return $this->policy->name. ': ' . $this->description;
    }
}