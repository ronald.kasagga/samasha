<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:52 PM
 */

namespace app\models;

class ImplementationLevel extends \app\models\base\ImplementationLevel
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['policy_statement_id'] = "Policy Statement";
        return $labels;
    }
}