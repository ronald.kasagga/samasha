<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/28/15
 * Time: 10:54 PM
 */

namespace app\models;

/**
 * @property string $fiscal_year
 */
class IndicatorFund extends \app\models\base\IndicatorFund
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['indicator_id'] = "Indicator";
        $labels['currency'] = "Currency (For the amount above)";
        $labels['amount_ugx'] = "Amount (UGX)";
        return $labels;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(($rate = Currency::find()->where(['code'=>$this->currency])->one()) != null)
                $this->amount_ugx = ($this->contribution_amount * $rate->ugx_exchange);
            return true;
        } else {
            return false;
        }
    }

    public function getAmountWithCurrency(){
        return $this->currency . '. ' . number_format($this->contribution_amount);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Added Indicator Funding for ' . $this->indicator->getDescription().' of '.$this->getAmountWithCurrency().' to '.$this->agency->name);

        parent::afterSave($insert, $changedAttributes);
    }
}