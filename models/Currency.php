<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/29/15
 * Time: 12:04 AM
 */

namespace app\models;


class Currency extends \app\models\base\Currency
{
    public static function getAnchors(){
        return [
            'Manage'=>['/currencies/index'],
            'Create'=>['/currencies/create'],
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['ugx_exchange'] = "Exchange Rate (to UGX)";
        return $labels;
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['ugx_exchange'], 'number'];
        return $rules;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Added Currency ' . $this->description. ' with the exchange rate of ugx.'. $this->ugx_exchange);
        else
            Log::record($this->tableName(), $this->id, Log::STATUS_UPDATE, 'Updated ' . $this->description. ' changed '. Log::concatAttributes($changedAttributes));

        parent::afterSave($insert, $changedAttributes);
    }

}