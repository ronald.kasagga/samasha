<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/28/15
 * Time: 11:23 PM
 */

namespace app\models;


class Initiative extends \app\models\base\Initiative
{
    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Created Initiative: ' . $this->name);
        else
            Log::record($this->tableName(), $this->id, Log::STATUS_UPDATE, 'Updated '.$this->name.' changed ' . Log::concatAttributes($changedAttributes));

        parent::afterSave($insert, $changedAttributes);
    }
}