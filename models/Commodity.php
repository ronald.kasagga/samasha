<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/29/15
 * Time: 3:13 AM
 */

namespace app\models;

/**
 * @property Donor $agency
 * @property Indicator $indicator
 * @property string $fiscal_year
*/


class Commodity extends \app\models\base\Commodity
{
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(($rate = Currency::find()->where(['code'=>$this->currency])->one()) != null)
                $this->expenditure_ugx = ($this->expenditure * $rate->ugx_exchange);
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['agency_id'] = "Partner";
        return $labels;
    }

    public function getAgency()
    {
        return $this->hasOne(Donor::className(), ['id' => 'agency_id']);
    }

    public function getIndicator()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator_id']);
    }

    public function getAgencyCommodity(){
        return $this->agency->name . ' (' . $this->name . ')';
    }
}