<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/27/15
 * Time: 4:31 PM
 */

namespace app\models;


class AppUser
{
    public static function getAnchors(){
        return [
            'List'=>['/user/admin/index'],
            'Create'=>['/user/admin/create'],
        ];
    }
}