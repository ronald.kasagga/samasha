<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:53 PM
 */

namespace app\models;


use yii\helpers\StringHelper;

class PerformanceIndicator extends \app\models\base\PerformanceIndicator
{

    public static function getStatuses()
    {
        return ['on_track', 'delayed', 'not_started'];
    }

    public static function getTimeFrameTypes()
    {
        return [
            'fiscal_year', 'date_range', 'month_periodic',
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['implementation_level_id'] = "Implementation Level";
        return $labels;

    }

    public function getShortImplementationLevel(){
        $desc = ImplementationLevel::findOne($this->implementation_level_id)->description;
        return StringHelper::truncateWords($desc, 10);
    }
}