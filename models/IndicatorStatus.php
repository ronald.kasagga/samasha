<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/29/15
 * Time: 3:29 AM
 */

namespace app\models;


use app\helper\FormHelper;

class IndicatorStatus extends \app\models\base\IndicatorStatus
{

    const STATUS_ACHIEVED = 'achieved';
    const STATUS_ON_TRACK = 'on_track';
    const STATUS_NOT_ACHIEVED = 'not_achieved';

    public static function getStatusOptions()
    {
        return [self::STATUS_ACHIEVED, self::STATUS_ON_TRACK, self::STATUS_NOT_ACHIEVED];
    }

    public static function getFiscalYearOptions($yearsOmitted = [])
    {
        $businessYears = [
            '2012/2013', '2013/2014', '2014/2015', '2015/2016', '2016/2017',
            '2017/2018', '2018/2019', '2019/2020',
        ];

        return array_diff($businessYears, $yearsOmitted);
    }

    public static function getStatusCode($status)
    {
        switch($status){
            case self::STATUS_ACHIEVED:
                return FormHelper::STATUS_SUCCESS;
            case self::STATUS_ON_TRACK:
                return FormHelper::STATUS_PENDING;
            case self::STATUS_NOT_ACHIEVED:
                return FormHelper::STATUS_FAILED;
        }
    }

    public static function getStatusHexColour($status)
    {
        switch($status){
            case IndicatorStatus::STATUS_NOT_ACHIEVED:
                return '#FF0000';
            case IndicatorStatus::STATUS_ON_TRACK:
                return '#FFAA31';
            case IndicatorStatus::STATUS_ACHIEVED:
                return '#008200';
        }
        return '#FF0000';
    }

    public static function validateStatus($status)
    {
        return in_array($status,
            [self::STATUS_ON_TRACK, self::STATUS_NOT_ACHIEVED, self::STATUS_ACHIEVED]
        )?$status:self::STATUS_NOT_ACHIEVED;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Added ' . $this->indicator->theme->name.' ' . $this->indicator->name.' status: '.$this->description);
        else
            Log::record($this->tableName(), $this->id, Log::STATUS_UPDATE, 'Updated ' . $this->indicator->name.' indicator status of ' . $this->indicator->theme->name . ', changed '.Log::concatAttributes($changedAttributes));

        parent::afterSave($insert, $changedAttributes);
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['description'] = 'Barriers';
        return $labels;
    }
}