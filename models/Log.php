<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/3/15
 * Time: 4:12 PM
 */

namespace app\models;
use app\helper\UserHelper;
use \app\models\base\Log as BaseLog;
use \app\models\search\Log as LogSearch;
use dektrium\user\models\User;
use Yii;


class Log extends BaseLog
{
    const STATUS_CREATE = 'created';
    const STATUS_UPDATE = 'update';

    public static function record($category, $object_id, $status, $message, $detail = null){
        $log = new Log();
        $log->object_category = $category;
        $log->object_id = ''.$object_id;
        $log->user_id = \Yii::$app->user->id;
        $log->status = $status;
        $log->message = $message;
        $log->detail = $detail;
        return $log->save();
    }

    public static function concatAttributes($changedAttributes)
    {
        $result = [];
        foreach($changedAttributes as $key => $attribute)
            $result[] = $key.' to '.$attribute;
        return implode(', ', $result);
    }

    public static function summaryDataProvider()
    {
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->summary(Yii::$app->request->queryParams);
        return $dataProvider;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getLabel(){
        switch($this->status){
            case 'create':
                return 'success';
            case 'update':
                return 'info';
        }
    }

    public function getColour(){
        switch($this->status){
            case 'create':
                return 'green';
            case 'update':
                return 'blue';
        }
    }

    public function getUserName(){
        return UserHelper::getUserFullName(User::findOne($this->user_id));
    }
}