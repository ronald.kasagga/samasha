<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:56 PM
 */

namespace app\models;


class Project extends \app\models\base\Project
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['policy_id'] = "Policy";
        $labels['budget_amount'] = "Amount (UGX)";
        return $labels;

    }
}