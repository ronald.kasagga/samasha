<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:58 PM
 */

namespace app\models;


class ProjectFund extends \app\models\base\ProjectFund
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['project_id'] = "Project";
        $labels['donor_id'] = "Donor";
        return $labels;

    }

    public function getFundDescription(){
        return $this->donor->name . ' (' . $this->project->name . ')';
    }
}