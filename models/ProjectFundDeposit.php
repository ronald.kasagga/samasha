<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:59 PM
 */

namespace app\models;

class ProjectFundDeposit extends \app\models\base\ProjectFundDeposit
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['fund_id'] = "Fund";
        return $labels;

    }
}