<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:52 PM
 */

namespace app\models;

use app\helper\FormHelper;
use yii\helpers\ArrayHelper;

class Indicator extends \app\models\base\Indicator
{
    const FISCAL_YEAR_POST_NAME = 'Achievement[fiscalYear]';
    private static $_currentYear;
    private static $_preferredYear;
    private static $_preferredMonth;

    public static function setPreferredYear($year)
    {
        self::$_preferredYear = $year;
    }

    public static function setPreferredMonth($month)
    {
        self::$_preferredMonth = $month;
    }

    private static function statusCode($status)
    {
        return IndicatorStatus::getStatusCode($status);
    }

    public function getDescription(){
        return $this->name . ' (' . $this->initiative->name . ')';
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['initiative_id'] = "Initiative";
        $labels['theme_id'] = "Theme";
        return $labels;
    }

    public function getNumberOfCommodities(){
        $command = "select count(*) from commodity where indicator_id = '{$this->id}'";
        return \Yii::$app->db->createCommand($command)->queryScalar();
    }

    public function getListFundingAgencies(){
        return implode(', ', ArrayHelper::map($this->indicatorFunds, 'id', 'agency.name'));
    }

    public function getStatus(){
        $status = $this->getStatusString();
        return self::getStatusBadge($status);
    }

    public static function getStatusBadge($status){
        return FormHelper::statusBadge(self::statusCode($status), FormHelper::expressString($status));
    }

    private static function sortStatuses($statuses){
        if(empty($statuses))
            return [];

        $result = [];
        foreach($statuses as $status){
            $key = $status['status'];
            if(!isset($result[$key]))
                $result[$key] = 0;
            $result[$key]++;
        }

        return $result;
    }

    private static function sortThemeStatuses($statuses){
        if(empty($statuses))
            return [];

        $result = [];
        foreach($statuses as $id => $status){
            if(!isset($result[$status]))
                $result[$status] = 0;
            $result[$status]++;
        }

        return $result;
    }

    public function getFinancialSources(){
        $sources = ArrayHelper::map(Commodity::find()->where(['indicator_id'=>$this->id])->all(), 'id', 'agencyCommodity');
        return implode(', ', $sources);
    }

    public function getFinancialSourcesCount(){
        return Commodity::find()->where(['indicator_id'=>$this->id])->count();
    }

    public function getLatestStatus(){
        $status = \app\models\IndicatorStatus::validateStatus($this->getStatusString());
        return FormHelper::statusBadge(self::statusCode($status), FormHelper::expressString($status));
    }

    public function getStatusString(){
        $fiscalYear = self::getCurrentFiscalYear();
        $not_started = IndicatorStatus::STATUS_NOT_ACHIEVED;
        $command = "SELECT ifnull(status, '$not_started') from indicator_status where indicator_id='{$this->id}' and fiscal_year='$fiscalYear' ORDER BY recorded_on DESC limit 1;";
        $status = \Yii::$app->db->createCommand($command)->queryScalar();
        return $status?$status:$not_started;
    }

    public function getStatusHexColour(){
        $status = $this->getStatusString();
        return IndicatorStatus::getStatusHexColour($status);
    }

    public function getLatestStatusDate(){
        $command = "select recorded_on from indicator_status where indicator_id='$this->id' ORDER BY recorded_on DESC limit 1";
        $date = \Yii::$app->db->createCommand($command)->queryScalar();
        return ($date != null)? $date : 'N/A';
    }

    public static function setCurrentYear($year){
        self::$_currentYear = $year;
    }

    public static function getCurrentFiscalYear($start_month=7)
    {
        $user_specified_year = isset($_POST['Achievement']['fiscalYear']);

        if($user_specified_year)
            self::$_currentYear = $_POST['Achievement']['fiscalYear'];

        else if(self::$_preferredYear != null)
            self::$_currentYear=self::$_preferredYear;

        else if(self::$_currentYear==null)
            self::$_currentYear=date('Y');

        $start_month = isset(self::$_preferredMonth) ?  self::$_preferredMonth : $start_month;

        $year = self::$_currentYear;
        $month = date('m');

        if($user_specified_year)
            return $year .'/'. ($year + 1);

        return ($month >= $start_month)
            ? $year .'/'. ($year + 1)
            : ($year - 1) .'/'. $year;
    }

    public function getRecordedFiscalYears(){
        $command = "select group_concat(fiscal_year) from indicator_status where indicator_id='{$this->id}'";
        $years = \Yii::$app->db->createCommand($command)->queryScalar();
        return explode(', ', $years);

    }

    public static function getThemeStatus(Policy $theme){
        $status = self::getThemeStatusString($theme);
        return self::getStatusBadge($status);
    }

    public static function getThemeStatusString(Policy $theme){
        $statuses = ArrayHelper::map($theme->indicators, 'id', 'statusString');

        if(empty($statuses))
            return IndicatorStatus::STATUS_NOT_ACHIEVED;

        $statuses = self::sortThemeStatuses($statuses);
        $status = array_keys($statuses, max($statuses))[0];
        return $status;
    }

    private static function getEntityStatus($command){
       $results = self::sortStatuses(\Yii::$app->db->createCommand($command)->queryAll());

        if(empty($results))
            return self::getStatusBadge(IndicatorStatus::STATUS_NOT_ACHIEVED);

        $status = array_keys($results, max($results))[0];
        return self::getStatusBadge($status);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Added ' . $this->theme->name.' indicator ' . $this->name);
        else
            Log::record($this->tableName(), $this->id, Log::STATUS_UPDATE, 'Updated ' . $this->theme->name.' indicator :' . $this->name,',changed '.Log::concatAttributes($changedAttributes));

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Commodity::deleteAll(['indicator_id'=>$this->id]);
            Contribution::deleteAll(['indicator_id'=>$this->id]);
            IndicatorFund::deleteAll(['indicator_id'=>$this->id]);
            IndicatorStatus::deleteAll(['indicator_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }

    public static function fiscalYears(){
        $years = [];
        foreach(range(2014, 2019) as $year)
            $years[$year] = $year.'/'.($year+1);

        return $years;
    }
}