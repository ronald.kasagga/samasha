<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Log as LogModel;

/**
 * Log represents the model behind the search form about `app\models\Log`.
 */
class Log extends LogModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['object_category', 'object_id', 'status', 'message', 'detail', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'object_category', $this->object_category])
            ->andFilterWhere(['like', 'object_id', $this->object_id])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'detail', $this->detail]);

        $query->orderBy = ['recorded_on'=>SORT_DESC];

        return $dataProvider;
    }

    public function summary($params){
        $dataProvider = $this->search($params);
        $query = $dataProvider->query;
        $query->limit(10);
        $dataProvider->query = $query;
        return $dataProvider;
    }
}
