<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/30/15
 * Time: 8:54 PM
 */

namespace app\models;


use yii\helpers\ArrayHelper;

class Contribution extends \app\models\base\Contribution
{
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['agency_id'] = "Funding Agency";
        $labels['indicator_id'] = "Indicator";
        return $labels;
    }

    public function getAgency()
    {
        return $this->hasOne(Donor::className(), ['id' => 'agency_id']);
    }

    public function getIndicator()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator_id']);
    }

    public function getAgencyContribution(){
        return $this->agency->name . ' (' . $this->name . ')';
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(),
                $this->id, Log::STATUS_CREATE,
                'Added Indicator Contribution for ' . $this->indicator->getDescription(). ' to '.$this->agency->name);

        parent::afterSave($insert, $changedAttributes);
    }

    public function rules(){
        $rules = parent::rules();
        $rules[] =[['contributed_on'], 'required'];
        return $rules;
    }

    public function getFiscalYear($startMonth=7){
        list($year, $month, $day) = explode('-', $this->contributed_on);
        $fiscal_year = ($year-1).'/'.$year;
        if($month >= $startMonth)
            $fiscal_year = $year.'/'.($year+1);
        return $fiscal_year;
    }
}