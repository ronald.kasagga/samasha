<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "implementation_level".
 *
 * @property integer $id
 * @property integer $policy_statement_id
 * @property string $description
 * @property string $recorded_on
 *
 * @property PolicyStatement $policyStatement
 */
class ImplementationLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'implementation_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['policy_statement_id', 'description'], 'required'],
            [['policy_statement_id'], 'integer'],
            [['recorded_on'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'policy_statement_id' => 'Policy Statement ID',
            'description' => 'Description',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicyStatement()
    {
        return $this->hasOne(PolicyStatement::className(), ['id' => 'policy_statement_id']);
    }
}
