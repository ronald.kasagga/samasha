<?php

namespace app\models\base;

use app\models\ProjectFund;
use Yii;

/**
 * This is the model class for table "project_fund_deposit".
 *
 * @property integer $id
 * @property integer $fund_id
 * @property double $amount
 * @property string $deposited_on
 * @property string $recorded_on
 *
 * @property ProjectFund $fund
 */
class ProjectFundDeposit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_fund_deposit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fund_id', 'amount', 'deposited_on'], 'required'],
            [['fund_id'], 'integer'],
            [['amount'], 'number'],
            [['deposited_on', 'recorded_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fund_id' => 'Fund ID',
            'amount' => 'Amount',
            'deposited_on' => 'Deposited On',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFund()
    {
        return $this->hasOne(ProjectFund::className(), ['id' => 'fund_id']);
    }
}
