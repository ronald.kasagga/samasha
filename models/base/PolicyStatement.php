<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "policy_statement".
 *
 * @property integer $id
 * @property string $initiative
 * @property string $description
 * @property string $recorded_on
 *
 * @property ImplementationLevel[] $implementationLevels
 */
class PolicyStatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'policy_statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiative', 'description'], 'required'],
            [['recorded_on'], 'safe'],
            [['initiative', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initiative' => 'Initiative',
            'description' => 'Description',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImplementationLevels()
    {
        return $this->hasMany(ImplementationLevel::className(), ['policy_statement_id' => 'id']);
    }
}
