<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "performance_indicator".
 *
 * @property integer $id
 * @property integer $implementation_level_id
 * @property string $description
 * @property string $time_from
 * @property string $time_to
 * @property string $timeframe_type
 * @property string $status
 * @property string $status_description
 * @property string $coalition_action
 * @property string $recorded_on
 *
 * @property PerformanceIndicator $implementationLevel
 * @property PerformanceIndicator[] $performanceIndicators
 */
class PerformanceIndicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'performance_indicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['implementation_level_id', 'description', 'status'], 'required'],
            [['implementation_level_id'], 'integer'],
            [['recorded_on'], 'safe'],
            [['description', 'time_from', 'time_to', 'timeframe_type', 'status', 'status_description', 'coalition_action'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'implementation_level_id' => 'Implementation Level ID',
            'description' => 'Description',
            'time_from' => 'Time From',
            'time_to' => 'Time To',
            'timeframe_type' => 'Timeframe Type',
            'status' => 'Status',
            'status_description' => 'Status Description',
            'coalition_action' => 'Coalition Action',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImplementationLevel()
    {
        return $this->hasOne(PerformanceIndicator::className(), ['id' => 'implementation_level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformanceIndicators()
    {
        return $this->hasMany(PerformanceIndicator::className(), ['implementation_level_id' => 'id']);
    }
}
