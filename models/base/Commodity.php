<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "commodity".
 *
 * @property integer $id
 * @property integer $indicator_id
 * @property integer $agency_id
 * @property string $name
 * @property string $description
 * @property double $expenditure
 * @property string $currency
 * @property double $expenditure_ugx
 * @property string $deposited_on
 * @property string $recorded_on
 */
class Commodity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commodity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'agency_id', 'name', 'expenditure', 'currency'], 'required'],
            [['indicator_id', 'agency_id'], 'integer'],
            [['description', 'currency'], 'string'],
            [['expenditure', 'expenditure_ugx'], 'number'],
            [['deposited_on', 'recorded_on'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator_id' => 'Indicator ID',
            'agency_id' => 'Agency ID',
            'name' => 'Name',
            'description' => 'Description',
            'expenditure' => 'Expenditure',
            'currency' => 'Currency',
            'expenditure_ugx' => 'Expenditure Ugx',
            'deposited_on' => 'Deposited On',
            'recorded_on' => 'Recorded On',
        ];
    }
}
