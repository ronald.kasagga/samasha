<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "donor".
 *
 * @property integer $id
 * @property string $name
 * @property string $added_on
 * @property string $description
 * @property string $contact_person
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $is_approved
 *
 * @property ProjectFund[] $projectFunds
 */
class Donor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'contact_person', 'contact_email', 'contact_phone'], 'required'],
            [['added_on', 'description'], 'safe'],
            [['name', 'contact_person', 'contact_email', 'contact_phone', 'is_approved'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'added_on' => 'Added On',
            'description' => 'Description',
            'contact_person' => 'Contact Person',
            'contact_email' => 'Contact Email',
            'contact_phone' => 'Contact Phone',
            'is_approved' => 'Is Approved',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectFunds()
    {
        return $this->hasMany(ProjectFund::className(), ['donor_id' => 'id']);
    }
}
