<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $policy_id
 * @property string $name
 * @property double $budget_amount
 * @property string $start_date
 * @property string $end_date
 * @property string $added_on
 *
 * @property Policy $policy
 * @property ProjectFund[] $projectFunds
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['policy_id', 'budget_amount', 'start_date', 'end_date', 'name'], 'required'],
            [['policy_id'], 'integer'],
            [['budget_amount'], 'number'],
            [['start_date', 'end_date', 'added_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'policy_id' => 'Policy ID',
            'name' => 'Name',
            'budget_amount' => 'Budget Amount',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'added_on' => 'Added On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'policy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectFunds()
    {
        return $this->hasMany(ProjectFund::className(), ['project_id' => 'id']);
    }
}
