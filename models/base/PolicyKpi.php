<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "policy_kpi".
 *
 * @property integer $id
 * @property integer $policy_id
 * @property string $description
 * @property string $recorded_on
 *
 * @property Policy $policy
 * @property PolicyKpiStatus[] $policyKpiStatuses
 */
class PolicyKpi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'policy_kpi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['policy_id', 'description'], 'required'],
            [['policy_id'], 'integer'],
            [['recorded_on'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'policy_id' => 'Policy ID',
            'description' => 'Description',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'policy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicyKpiStatuses()
    {
        return $this->hasMany(PolicyKpiStatus::className(), ['policy_kpi_id' => 'id']);
    }
}
