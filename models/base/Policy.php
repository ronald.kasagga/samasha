<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "policy".
 *
 * @property integer $id
 * @property string $name
 * @property string $added_on
 *
 * @property PolicyKpi[] $policyKpis
 * @property Project[] $projects
 */
class Policy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'policy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['added_on'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'added_on' => 'Added On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicyKpis()
    {
        return $this->hasMany(PolicyKpi::className(), ['policy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['policy_id' => 'id']);
    }
}
