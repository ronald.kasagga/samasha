<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "indicator_status".
 *
 * @property integer $id
 * @property integer $indicator_id
 * @property string $status
 * @property string $description
 * @property string $fiscal_year
 * @property string $recorded_on
 *
 * @property Indicator $indicator
 */
class IndicatorStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'status', 'description', 'fiscal_year'], 'required'],
            [['indicator_id'], 'integer'],
            [['recorded_on'], 'safe'],
            [['status', 'description', 'fiscal_year'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator_id' => 'Indicator ID',
            'status' => 'Status',
            'description' => 'Description',
            'fiscal_year' => 'Fiscal Year',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator_id']);
    }
}
