<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "initiative".
 *
 * @property integer $id
 * @property string $name
 * @property string $added_on
 * @property string $initiative
 *
 * @property Indicator[] $indicators
 */
class Initiative extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'initiative';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['added_on', 'link'], 'safe'],
            [['link'], 'url'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'added_on' => 'Added On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicators()
    {
        return $this->hasMany(Indicator::className(), ['initiative_id' => 'id']);
    }
}
