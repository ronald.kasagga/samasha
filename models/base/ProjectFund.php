<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "project_fund".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $donor_id
 * @property double $contribution_amount
 * @property string $date_from
 * @property string $date_to
 * @property string $added_on
 *
 * @property Donor $donor
 * @property Project $project
 * @property ProjectFundDeposit[] $projectFundDeposits
 */
class ProjectFund extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_fund';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'donor_id', 'contribution_amount'], 'required'],
            [['project_id', 'donor_id'], 'integer'],
            [['contribution_amount'], 'number'],
            [['date_from', 'date_to', 'added_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'donor_id' => 'Donor ID',
            'contribution_amount' => 'Contribution Amount',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'added_on' => 'Added On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectFundDeposits()
    {
        return $this->hasMany(ProjectFundDeposit::className(), ['fund_id' => 'id']);
    }
}
