<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property string $object_category
 * @property string $object_id
 * @property integer $user_id
 * @property string $status
 * @property string $message
 * @property string $detail
 * @property string $recorded_on
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_category', 'object_id', 'status', 'message'], 'required'],
            [['user_id'], 'integer'],
            [['detail'], 'string'],
            [['recorded_on'], 'safe'],
            [['object_category', 'object_id', 'status', 'message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_category' => 'Object Category',
            'object_id' => 'Object ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'message' => 'Message',
            'detail' => 'Detail',
            'recorded_on' => 'Recorded On',
        ];
    }
}
