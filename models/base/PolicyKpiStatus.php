<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "policy_kpi_status".
 *
 * @property integer $id
 * @property integer $policy_kpi_id
 * @property string $description
 * @property string $status
 * @property string $fiscal_year
 * @property string $recorded_on
 *
 * @property PolicyKpi $policyKpi
 */
class PolicyKpiStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'policy_kpi_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['policy_kpi_id', 'description', 'status', 'fiscal_year'], 'required'],
            [['policy_kpi_id'], 'integer'],
            [['recorded_on'], 'safe'],
            [['description', 'status', 'fiscal_year'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'policy_kpi_id' => 'Policy Kpi ID',
            'description' => 'Description',
            'status' => 'Status',
            'fiscal_year' => 'Fiscal Year',
            'recorded_on' => 'Recorded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicyKpi()
    {
        return $this->hasOne(PolicyKpi::className(), ['id' => 'policy_kpi_id']);
    }
}
