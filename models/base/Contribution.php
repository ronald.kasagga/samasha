<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "contribution".
 *
 * @property integer $id
 * @property integer $indicator_id
 * @property integer $agency_id
 * @property string $name
 * @property string $description
 * @property string $contributed_on
 * @property string $recorded_on
 */
class Contribution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contribution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'agency_id', 'name'], 'required'],
            [['indicator_id', 'agency_id'], 'integer'],
            [['description'], 'string'],
            [['contributed_on', 'recorded_on'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator_id' => 'Indicator ID',
            'agency_id' => 'Agency ID',
            'name' => 'Name',
            'description' => 'Description',
            'contributed_on' => 'Contributed On',
            'recorded_on' => 'Recorded On',
        ];
    }
}
