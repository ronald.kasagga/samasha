<?php

namespace app\models\base;

use Yii;
use app\models\Policy as Theme;

/**
 * This is the model class for table "indicator".
 *
 * @property integer $id
 * @property string $recorded_on
 * @property integer $theme_id
 * @property integer $initiative_id
 * @property string $name
 *
 * @property Initiative $initiative
 * @property Policy $theme
 * @property IndicatorFund[] $indicatorFunds
 */
class Indicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recorded_on'], 'safe'],
            [['theme_id', 'initiative_id', 'name'], 'required'],
            [['theme_id', 'initiative_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recorded_on' => 'Recorded On',
            'theme_id' => 'Theme ID',
            'initiative_id' => 'Initiative ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitiative()
    {
        return $this->hasOne(Initiative::className(), ['id' => 'initiative_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorFunds()
    {
        return $this->hasMany(IndicatorFund::className(), ['indicator_id' => 'id']);
    }
}
