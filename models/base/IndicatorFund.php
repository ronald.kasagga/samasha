<?php

namespace app\models\base;

use app\models\Donor as ChildDonor;
use app\models\Indicator as ChildIndicator;
use Yii;

/**
 * This is the model class for table "indicator_fund".
 *
 * @property integer $id
 * @property integer $indicator_id
 * @property integer $agency_id
 * @property double $contribution_amount
 * @property string $currency
 * @property string $amount_ugx
 * @property string $added_on
 *
 * @property ChildDonor$agency
 * @property Indicator $indicator
 */
class IndicatorFund extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_fund';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'agency_id', 'contribution_amount', 'currency'], 'required'],
            [['indicator_id', 'agency_id'], 'integer'],
            [['contribution_amount'], 'number'],
            [['added_on'], 'safe'],
            [['currency', 'amount_ugx'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator_id' => 'Indicator ID',
            'agency_id' => 'Agency ID',
            'contribution_amount' => 'Contribution Amount',
            'currency' => 'Currency',
            'amount_ugx' => 'Amount Ugx',
            'added_on' => 'Added On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(ChildDonor::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator()
    {
        return $this->hasOne(ChildIndicator::className(), ['id' => 'indicator_id']);
    }
}
