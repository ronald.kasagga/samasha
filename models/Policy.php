<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:54 PM
 */

namespace app\models;

class Policy extends \app\models\base\Policy
{
    public function getNumberOfIndicators(){
        return Indicator::find()->where(['theme_id'=>$this->id])->count();
    }

    public function getNumberOfFundingSources(){
        $query = "select count(agency_id) from indicator_fund where indicator_id in (select id from indicator where theme_id = '{$this->id}')";
        return \Yii::$app->db->createCommand($query)->queryScalar();
    }

    public function getColouredStatus(){
        return Indicator::getThemeStatus($this);
    }

    public function getStatusColour(){
        $status = Indicator::getThemeStatusString($this);
        IndicatorStatus::getStatusHexColour($status);
    }

    public function getIndicators()
    {
        return $this->hasMany(Indicator::className(), ['theme_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Indicator::deleteAll(['theme_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function getIcon()
    {
        switch(trim(strtolower($this->name))){
            case 'finance':
                return 'ico-dollar';
            case 'policy':
                return 'ico-pen';
            case 'service delivery':
                return 'ico-exchange';
            case 'supply chain':
                return 'ico-cog-3';
            case 'technology':
                return 'ico-laptop';
            default:
                return 'ico-folder-open';
        }
    }

}