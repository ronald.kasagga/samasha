<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/24/15
 * Time: 9:51 PM
 */

namespace app\models;


use app\helper\FormHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;

class Donor extends \app\models\base\Donor
{
    const STATUS_APPROVED = 'approved';
    const STATUS_NOT_APPROVED = 'not_approved';
    public static $theme_id_search;

    public static function getActivePartners($indicator_id)
    {
        $fiscalYear = Indicator::getCurrentFiscalYear();
        $fiscalYearStartMonth = 7;

        $partner_ids = [];
        $contribution_query =
            "SELECT
              CASE WHEN MONTH(contributed_on)>=$fiscalYearStartMonth THEN
                concat(YEAR(contributed_on), '/',YEAR(contributed_on)+1)
              ELSE concat(YEAR(contributed_on)-1,'/', YEAR(contributed_on)) END AS fiscal_year, agency_id
            FROM contribution where indicator_id = '$indicator_id'";

        $contribution_data = \Yii::$app->db->createCommand($contribution_query)->queryAll();
        foreach($contribution_data as $row)
            if($row['fiscal_year'] == $fiscalYear)
                $partner_ids[] = $row['agency_id'];

        $funding_query =
            "select agency_id
                from indicator_fund
                where indicator_id = '$indicator_id' and fiscal_year = '$fiscalYear'";
        $funding_data = \Yii::$app->db->createCommand($funding_query)->queryAll();
        foreach($funding_data as $row)
            $partner_ids[] = $row['agency_id'];

        return Donor::find()->where(['id'=>$partner_ids])->all();
    }

    public function getDonorLink(){
        return Html::a($this->name, ['/donors/donor/view', 'id'=>$this->id]);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['contact_email'],'email'];
        $rules[] = [['is_approved'], 'default', 'value'=>self::STATUS_NOT_APPROVED,];
        $rules[] = [['is_approved'], 'in', 'range'=>self::getStatuses(), 'strict'=>true];

        return $rules;
    }

    public function attributeLabels(){
        $labels = parent::attributeLabels();
        $labels['is_approved'] = 'Approval Status';
        return $labels;
    }

    public static function getStatuses(){
        return [
            self::STATUS_NOT_APPROVED, self::STATUS_APPROVED
        ];
    }

    public function getVerboseApproval(){
        $statusMap = FormHelper::toAssociativeArray(self::getStatuses());
        return isset($statusMap[$this->is_approved]) ? $statusMap[$this->is_approved] : $this->is_approved;
    }

    public function getTruncatedDescription(){
        return StringHelper::truncateWords($this->description, 8);
    }

    public function indicatorFundCount($theme_id = null)
    {
        $theme_id = ($theme_id) ? $theme_id : self::$theme_id_search;
        $command = "select count(indicator_id) from commodity where indicator_id in (select id from `indicator` where theme_id = '$theme_id') and agency_id='$this->id'";
        return \Yii::$app->db->createCommand($command)->queryScalar();
    }

    public function indicatorContributionsCount($theme_id = null)
    {
        $theme_id = ($theme_id) ? $theme_id : self::$theme_id_search;
        $command = "select count(indicator_id) from contribution where indicator_id in (select id from `indicator` where theme_id = '$theme_id') and agency_id='$this->id'";
        return \Yii::$app->db->createCommand($command)->queryScalar();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
            Log::record($this->tableName(), $this->id, Log::STATUS_CREATE, 'Created Partner: ' . $this->name);
        else
            Log::record($this->tableName(), $this->id, Log::STATUS_UPDATE, 'Updated '.$this->name.' changed ' . Log::concatAttributes($changedAttributes));

        parent::afterSave($insert, $changedAttributes);
    }

    public function getContributionStatistics($indicator_id){
        $command = "select count(id) count, sum(amount_ugx) as amount from indicator_fund where indicator_id='$indicator_id' and agency_id='{$this->id}';";
        $result = \Yii::$app->db->createCommand($command)->cache(5)->queryOne();
        return $result;
    }

    public function getCommodityStatistics($indicator_id)
    {
        $command = "select count(id) count from contribution where indicator_id='$indicator_id' and agency_id='{$this->id}';";
        $result = \Yii::$app->db->createCommand($command)->cache(5)->queryScalar();
        return $result;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Commodity::deleteAll(['agency_id'=>$this->id]);
            Contribution::deleteAll(['agency_id'=>$this->id]);
            IndicatorFund::deleteAll(['agency_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $indicator_id
     * @return \yii\db\ActiveRecord[]|Commodity[]
     */
    public function getContributions($indicator_id)
    {
        return Contribution::find()->where([
            'agency_id'=>$this->id,
            'indicator_id'=>$indicator_id,
        ])->all();
    }

    /**
     * @param $indicator_id
     * @return \yii\db\ActiveRecord[]|IndicatorFund[]
     */
    public function getFunding($indicator_id)
    {
        return IndicatorFund::find()->where([
            'agency_id'=>$this->id,
            'indicator_id'=>$indicator_id,
        ])->all();
    }


}