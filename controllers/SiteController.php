<?php

namespace app\controllers;

use app\helper\UserHelper;
use app\models\Log;
use app\modules\reports\models\Theme;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'themesDataProvider'=>Theme::instantDataProvider(),
            'logsDataProvider'=>Log::summaryDataProvider()
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionCalendar(){
        $calendarSampleJson = '[{"id":"1","title":"Ajax event (:","start":1436077980,"className":"green"},{"id":"2","title":"Ajax long event","start":1436337180,"end":1436941980,"className":"red"},{"id":"3","title":"Me gusta","start":1437201180,"className":"orange"},{"id":"4","title":"Today :)","start":1437978780},{"id":"5","title":"Make an amazing template","start":1437892380,"end":1438151580,"className":"purple"}]';
        \Yii::$app->response->format = Response::FORMAT_JSON;
        echo $calendarSampleJson;
        //return $this->renderPartial('calendar');
    }

    public function actionGrantUser($username)
    {
        UserHelper::addToAdmin($username);
        return $this->redirect(['/user/admin']);
    }

    public function actionRevokeUser($username)
    {
        UserHelper::removeFromAdmin($username);
        return $this->redirect(['/user/admin']);
    }
}
