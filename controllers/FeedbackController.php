<?php

namespace app\controllers;

use app\helper\ExportHelper;
use Yii;
use app\models\Feedback;
use app\models\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\RangeNotSatisfiableHttpException;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    public $defaultAction='create';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goBack();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionExport(){
        $content = join("\t", (new Feedback())->attributeLabels()) . "\n";
        $i = ['limit'=>20, 'offset'=>0, 'total'=>Feedback::find()->count()];
        for($i['offset'] = 0; $i['offset'] < $i['total']; $i['offset'] += $i['limit']){
            foreach (Feedback::find()->limit($i['limit'])->offset($i['offset'])->all() as $row)
                $content .= join("\t", $row->attributes) . "\n";
        }
        \Yii::$app->response->sendContentAsFile($content, "user feedback.csv");
    }
}
