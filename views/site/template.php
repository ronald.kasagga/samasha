<?php
/* @var $this yii\web\View */
$this->title = 'Home';
$this->params['breadcrumbs'][] = '';
?>
<div class="col-md-7">

    <div class="block">
        <div class="head">
            <div class="icon"><span class="ico-chart-4"></span></div>
            <h2>Visitor Statistics</h2>
            <ul class="buttons">
                <li><a href="#" class="ublock"><div class="icon"><span class="ico-undo"></span></div></a></li>
                <li><a href="#" class="cblock"><div class="icon"><span class="ico-sort"></span></div></a></li>
            </ul>
        </div>
        <div class="data">
            <div id="main_chart" style="height: 300px;"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="stat">
                    <span class="cdblue">2,350</span> PAGE<br/> VIEWS
                </div>
            </div>
            <div class="col-md-4">
                <div class="stat">
                    <span class="cblue">1,120</span> UNIQUE<br/> PAGE VIEWS
                </div>
            </div>
            <div class="col-md-4">
                <div class="stat">
                    <span class="corange">720</span> SALES<br/> PER PERIOD
                </div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="data-fluid">
            <div id="main_calendar"></div>
        </div>
    </div>

</div>

<div class="col-md-5">

    <div class="block">
        <div class="head">
            <div class="icon"><span class="ico-tag"></span></div>
            <h2>Tickets</h2>
            <ul class="buttons">
                <li><a href="#" onClick="source('tickets');
                                                return false;"><div class="icon"><span class="ico-info"></span></div></a></li>
                <li><a href="#" class="ublock"><div class="icon"><span class="ico-undo"></span></div></a></li>
                <li><a href="#" class="cblock"><div class="icon"><span class="ico-sort"></span></div></a></li>
            </ul>
        </div>
        <div class="data-fluid">
            <table width="100%" class="table tickets">
                <tr>
                    <td width="55" class="bl_blue"><span class="label label-info">new</span></td>
                    <td width="50">#AA-325 <span class="mark">23/02/2013</span></td>
                    <td><a href="#" class="cblue">Buy on themeforest this great template...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_blue"><span class="label label-info">new</span></td>
                    <td>#AA-216 <span class="mark">22/02/2013</span></td>
                    <td><a href="#" class="cblue">Go to shop and buy beer!</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_green"><span class="label label-success">done</span></td>
                    <td>#AC-857 <span class="mark">21/02/2013</span></td>
                    <td><a href="#" class="cgreen">Buy on themeforest this great template...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_red"><span class="label label-important">removed</span></td>
                    <td>#VB-57 <span class="mark">20/02/2013</span></td>
                    <td><a href="#" class="cred">Buy something for my dog...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_red"><span class="label label-important">removed</span></td>
                    <td>#VB-44 <span class="mark">20/02/2013</span></td>
                    <td><a href="#" class="cred">Buy something for my dog...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>

                <tr>
                    <td class="bl_green"><span class="label label-success">done</span></td>
                    <td>#AA-216 <span class="mark">22/02/2013</span></td>
                    <td><a href="#" class="cgreen">Go to shop and buy beer!</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_red"><span class="label label-important">removed</span></td>
                    <td>#VB-31 <span class="mark">21/02/2013</span></td>
                    <td><a href="#" class="cred">Buy on themeforest this great template...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
                <tr>
                    <td class="bl_green"><span class="label label-success">done</span></td>
                    <td>#VB-57 <span class="mark">20/02/2013</span></td>
                    <td><a href="#" class="cgreen">Buy something for my dog...</a> <span class="mark">Added by Dmitry Ivaniuk</span></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="widgets">

        <a href="#" class="swidget blue">
            <div class="icon">
                <span class="ico-box"></span>
            </div>
            <div class="bottom">
                <div class="text">Products</div>
            </div>
        </a>

        <a href="#" class="swidget green">
            <div class="epc mini">
                <span class="epcm-green" data-percent="81">81%</span>
            </div>
            <div class="bottom">
                <div class="text">Statistic</div>
            </div>
        </a>

        <a href="#" class="swidget orange">
            <div class="icon">
                <span class="ico-info"></span>
            </div>
            <div class="bottom">
                <div class="text">Info</div>
                <div class="value">2,312</div>
            </div>
        </a>

    </div>

    <div class="block">
        <div class="head dblue">
            <h2>Messages</h2>
            <ul class="buttons">
                <li><a href="#" onClick="source('messages');
                                                return false;"><div class="icon"><span class="ico-info"></span></div></a></li>
                <li><a href="#" class="ublock"><div class="icon"><span class="ico-undo"></span></div></a></li>
                <li><a href="#" class="cblock"><div class="icon"><span class="ico-sort"></span></div></a></li>
            </ul>
        </div>
        <div class="data dark npr npb">
            <div class="messages scroll" style="height: 200px;">
                <div class="item blue">
                    <div class="arrow"></div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat.</div>
                    <div class="date">09.02., 21:04</div>
                </div>
                <div class="item dblue out">
                    <div class="arrow"></div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat.</div>
                    <div class="date">09.02., 21:02</div>
                </div>
                <div class="item blue">
                    <div class="arrow"></div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat.</div>
                    <div class="date">09.02., 20:55</div>
                </div>
                <div class="item dblue out">
                    <div class="arrow"></div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat.</div>
                    <div class="date">09.02., 21:02</div>
                </div>
                <div class="item blue">
                    <div class="arrow"></div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat.</div>
                    <div class="date">09.02., 20:55</div>
                </div>
            </div>
        </div>
        <div class="toolbar dark">
            <div class="input-group">
                <span class="input-group-addon dblue"><span class="glyphicon glyphicon-envelope"></span></span>
                <input class="form-control" type="text"/>
                <div class="input-group-btn">
                    <button class="btn dblue" type="button">Send  <span class="icon-arrow-next icon-white"></span></button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="head blue">
                <div class="icon"><i class="ico-layout-9"></i></div>
                <h2>Default table sorting</h2>
                <ul class="buttons">
                    <li><a href="#" onClick="source('table_sort');
                                                return false;"><div class="icon"><span class="ico-info"></span></div></a></li>
                </ul>
            </div>
            <div class="data-fluid">
                <table class="table" cellpadding="0" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"><input type="checkbox" class="checkall"/></th>
                        <th width="20%">Name</th>
                        <th>Product</th>
                        <th width="20%">Status</th>
                        <th width="20%">Date</th>
                        <th width="100" class="TAC">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="checkbox" name="order[]" value="528"/></td>
                        <td><a href="#">Dmitry Ivaniuk</a></td>
                        <td>Product #212</td>
                        <td><span class="label label-important">New</span></td>
                        <td>24/11/2012</td>
                        <td>
                            <a href="#" class="button green">
                                <div class="icon"><span class="ico-pencil"></span></div>
                            </a>
                            <a href="#" class="button red">
                                <div class="icon"><span class="ico-remove"></span></div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="order[]" value="527"/></td>
                        <td><a href="#">John Doe</a></td>
                        <td>Product #132</td>
                        <td><span class="label label-important">New</span></td>
                        <td>24/11/2012</td>
                        <td>
                            <a href="#" class="button green">
                                <div class="icon"><span class="ico-pencil"></span></div>
                            </a>
                            <a href="#" class="button red">
                                <div class="icon"><span class="ico-remove"></span></div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="order[]" value="526"/></td>
                        <td><a href="#">Alex Fruz</a></td>
                        <td>Product #53</td>
                        <td><span class="label label-important">New</span></td>
                        <td>23/11/2012</td>
                        <td>
                            <a href="#" class="button green">
                                <div class="icon"><span class="ico-pencil"></span></div>
                            </a>
                            <a href="#" class="button red">
                                <div class="icon"><span class="ico-remove"></span></div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="order[]" value="525"/></td>
                        <td><a href="#">Olga Yukhimchuk</a></td>
                        <td>Product #874</td>
                        <td><span class="label label-info">Pending</span></td>
                        <td>23/11/2012</td>
                        <td>
                            <a href="#" class="button green">
                                <div class="icon"><span class="ico-pencil"></span></div>
                            </a>
                            <a href="#" class="button red">
                                <div class="icon"><span class="ico-remove"></span></div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="order[]" value="524"/></td>
                        <td><a href="#">Angelina Rose</a></td>
                        <td>Product #533</td>
                        <td><span class="label label-info">Pending</span></td>
                        <td>23/11/2012</td>
                        <td>
                            <a href="#" class="button green">
                                <div class="icon"><span class="ico-pencil"></span></div>
                            </a>
                            <a href="#" class="button red">
                                <div class="icon"><span class="ico-remove"></span></div>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="dialog" id="source" style="display: none;" title="Source"></div>

<div id="fcAddEvent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="fcAddEventLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="fcAddEventLabel">Add new event</h3>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-3">Title:</div>
            <div class="col-md-9"><input class="form-control" type="text" id="fcAddEventTitle"/></div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="fcAddEventButton">Add</button>
    </div>
</div>
