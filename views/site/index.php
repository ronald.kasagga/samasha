<?php
/* @var $this yii\web\View */
use app\helper\FormHelper;
use app\models\Indicator;
use app\models\IndicatorStatus;
use app\models\Policy;
use yii\helpers\Url;

$this->title = 'Uganda Commitment Initiative';
$this->params['breadcrumbs'][] = '';?>
<div class="col-md-12">
    <div  class="block">
        <div class="head">
            <div class="row">
                <i class="icon pull-left"><span class="ico-chart-4"></span></i>
                <h2 class="col-md-7">
                    <?=FormHelper::fiscalYearForm()?>
                    &nbsp;Progress made towards achievement of commitments
                </h2>
            </div>
            <span class="row">Click an indicator section for more detail</span>
            <div class="center-block">
                <div class="text-center">
                    <?php foreach(
                        [IndicatorStatus::STATUS_ACHIEVED, IndicatorStatus::STATUS_ON_TRACK, IndicatorStatus::STATUS_NOT_ACHIEVED]
                        as $status):?>
                            <span class="label"
                                  style="margin: 0 4px; background: <?=IndicatorStatus::getStatusHexColour($status)?> !important;">
                                <?=FormHelper::expressString($status)?>
                            </span>
                    <?php endforeach?>
                </div>
            </div>
            <ul class="buttons">
                <li><a href="<?=Url::toRoute('/reports/themes')?>" ><div class="icon"><span class="ico-link"></span></div></a></li>
            </ul>
        </div>
        <div class="data-fluid">
            <?php foreach(Policy::find()->all() as $theme):?>
                <?=$this->render('//theme/summary/_linear', ['theme'=>$theme]);?>
            <?php endforeach?>
        </div>

    </div>
</div>

