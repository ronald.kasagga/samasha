<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
$isLoginPage = (Yii::$app->user->loginUrl[0] == ('/'.Yii::$app->controller->route));
$isRecoveryPage = ('user/recovery/request' == (Yii::$app->controller->route));
$isRsendPage = ('user/registration/resend' == (Yii::$app->controller->route));
$isHomePage = ('site/index' == (Yii::$app->controller->route));


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) . ' - '. Yii::$app->name ?></title>
    <?=Yii::$app->controller->renderPartial('@app/views/layouts/favicon')?>
    <?php $this->head() ?>
    <?=Yii::$app->controller->renderPartial('@app/views/layouts/css')?>
</head>
<body>

<?php $this->beginBody() ?>
    <?php if($isLoginPage || $isRecoveryPage || $isRsendPage)
        echo '<div class="container">'.$content.'</div>';
    else{?>
    <div class="wrapper">
        <?=Yii::$app->controller->renderPartial('//layouts/navigation-sidebar')?>
        <?php Yii::$app->controller->renderPartial('//layouts/navigation-top')?>

        <div class="body">
            <?php if(Yii::$app->controller->id != 'feedback'):?>
            <div class="row">
                <div class="alert alert-success col-md-12" onclick="window.location='<?=Url::to(['/feedback'])?>'">
                    <?=\app\helper\TimeHelper::greet()?> (ツ). Please take a moment to review this motion tracker.
                    Thank You ☺
                </div>
            </div>
            <?php endif;?>
            <?= Yii::$app->controller->renderPartial('//layouts/navigation-top')?>
            <div class="container">
                <div class="page-header">
                    <?= Yii::$app->controller->renderPartial('//layouts/page-header', ['main_layout'=>$this])?>
                    <?= Yii::$app->controller->renderPartial('//layouts/breadcrumbs', ['main_layout'=>$this])?>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="container col-lg-12">
                <div id="notifications"></div>
                <?= $content ?>
            </div>
        </div>
    </div>
    <?php }?>



<?php $this->endBody() ?>
<?=Yii::$app->controller->renderPartial('@app/views/layouts/scripts')?>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/select/select2.min.js'></script>
<?=($isHomePage)?Yii::$app->controller->renderPartial('//layouts/chart_scripts'):''?>
<script type='text/javascript'>
    function postLink(url, data){
        addNotification('Please Wait', 'info');
        //window.location.replace(url);
        history.pushState({}, '', url);
        $('body').load(url, data);
    }

    function addNotification(message, type){
        var notification = buildNotification(message, type);
        $('#notifications').append(notification);
    }

    function buildNotification(message, type){
        return '<div class="alert alert-'+ type+' alert-dismissible" role="alert">' +
                    '<span class="text-center">' + message + '</span>' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                '</div>';
    }
    $(document).ready(function(){
        adjustMenusAndBody();
    });

    $(window).resize(function(){
        adjustMenusAndBody();
    });

    function adjustMenusAndBody(){
        var width = window.innerWidth;
        if(width < 1024){
            $('.top-nav-item').show();
            $('.sidebar').hide();
            $('.body').css('margin-left', '0');
        }else{
            $('.top-nav-item').hide();
            $('.sidebar').show();
            $('.body').css('margin-left', '250px');
        }
    }
</script>

</body>
</html>
<?php $this->endPage() ?>
