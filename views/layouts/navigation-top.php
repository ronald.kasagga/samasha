<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 12:22 PM
 */
use app\helper\NavHelper;
use app\helper\UserHelper;
use app\modules\donors\Module as DonorModule;
use app\modules\projects\Module as ProjectsModule;
use app\modules\commitments\Module as CommitmentsModule;
use yii\helpers\Html;
use yii\helpers\Url; ?>

<ul class="navigation" >
    <li class="top-nav-item">
        <a href="<?=Url::home()?>" class="button">
            <div class="icon">
                <span class="ico-home"></span>
            </div>
            <div class="name">Dashboard</div>
        </a>
    </li>
    <li class="top-nav-item">
        <a href="<?=Url::toRoute('/donors')?>" class="button yellow">
            <div class="icon">
                <span class="ico-money-bag"></span>
            </div>
            <div class="name">Partners</div>
        </a>
    </li>
    <li class="top-nav-item">
        <a href="<?=Url::toRoute('/reports/themes')?>" class="button orange">
            <div class="icon">
                <span class="ico-cog"></span>
            </div>
            <div class="name">Theme Reports</div>
        </a>
    </li>
    <?php if(UserHelper::isAdmin()):?>
    <li class="top-nav-item">
        <a href="<?=Url::toRoute('/currencies')?>" class="button blue">
            <div class="icon">
                <span class="ico-money"></span>
            </div>
            <div class="name">Currencies</div>
        </a>
    </li>
    <?php endif?>
    <li class="top-nav-item">
        <a href="<?=Url::toRoute('/commitments/initiatives')?>" class="button green">
            <div class="icon">
                <span class="ico-atom"></span>
            </div>
            <div class="name">Initiatives</div>
        </a>
    </li>
    <?php if(UserHelper::isAdmin()):?>
    <li class="top-nav-item">
        <a href="#" class="button purple">
            <div class="icon">
                <span class="ico-user"></span>
            </div>
            <div class="name">Users</div>
        </a>
    </li>
    <?php endif?>
    <li>
        <div class="user">
            <div class="sbutton blue">
                <img src="<?=Url::to(UserHelper::getUserPicUrl())?>" align="left">
                <?php if(!Yii::$app->user->isGuest){?>
                    <a href="#" class="name">
                        <span><?=NavHelper::getUserDisplayName()?></span>
                        <span class="sm">Administrator</span>
                    </a>
                    <div class="popup" style="width: 150px">
                        <div class="arrow"></div>
                        <div class="row">
                            <div class="row-form" style="border-bottom: 1px solid #cecece">
                                <div class="col-md-4"><strong><?=ucwords(Yii::$app->user->identity->username)?></strong></div>
                            </div>
                            <div class="row-form">
                                <div class="col-md-4"><?=Html::a('Account', ['/user/settings/account'])?></div>
                            </div>
                            <div class="row-form">
                                <div class="col-md-4"><?=Html::a('Profile', ['/user/profile/show', 'id'=>Yii::$app->user->id])?></div>
                            </div>
                            <div class="row-form">
                                <div class="col-md-4"><?=Html::a('Settings', ['/user/settings/profile'])?></div>
                            </div>
                            <div class="row-form" style="border-top: 1px solid #cecece">
                                <div class="col-md-4"><?=Html::a('Logout', ['/user/security/logout'], ['data-method'=>'post'])?></div>
                            </div>

                        </div>
                    </div>
                <?php }else{?>
                    <a href="<?=Url::to(Yii::$app->user->loginUrl)?>" class="name">
                        <span>Login</span>
                        <span class="sm">Guest User</span>
                    </a>
                <?php } ?>
            </div>
        </div>
    </li>
</ul>
