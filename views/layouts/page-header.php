<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 12:26 PM
 * @var $main_layout \yii\web\View
 */?>
<div class="wrap-title">
    <div class="icon">
        <span class="ico-arrow-right"></span>
    </div>
    <h1><?=$main_layout->title?> <small><?=Yii::$app->name?></small></h1>
</div>
