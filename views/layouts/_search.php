<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/31/15
 * Time: 12:25 PM
 */
use yii\helpers\Html;
$donorSearchTerm = isset($_REQUEST['Donor']['name']) ? $_REQUEST['Donor']['name'] : null;
?>
<?=Html::beginForm(['/donors/donor/index'], 'GET')?>
<div class="input-group">
    <span class="input-group-addon orange"><span class="glyphicon glyphicon-search"></span></span>
    <?=Html::textInput('Donor[name]', $donorSearchTerm, ['class'=>'form-control', 'placeholder'=>'search partner ...'])?>
</div>
<?=Html::endForm()?>

