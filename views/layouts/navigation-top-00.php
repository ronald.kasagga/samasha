<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 12:09 PM
 */
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar; ?>

<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
        Yii::$app->user->isGuest ?
            ['label' => 'Login', 'url' => Yii::$app->user->loginUrl] :
            [
                'label' => 'Hello ' . Yii::$app->user->identity->username,
                'items'=> [
                    ['label'=>'Account', 'url' => ['/user/settings/account']],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Your Profile</li>',
                    ['label'=>'View', 'url' => ['/user/profile/show', 'id'=>Yii::$app->user->id]],
                    ['label'=>'Settings', 'url' => ['/user/settings/profile']],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Social Networks</li>',
                    ['label'=>'Edit', 'url' => ['/user/settings/networks']],
                    '<li class="divider"></li>',
                    ['label'=>'Log Out', 'url' => ['/user/security/logout'],'linkOptions' => ['data-method' => 'post']]
                ],
            ],
        Yii::$app->user->isGuest ?
            ['label' => 'Register', 'url' => ['/user/registration/register']] :['label'=>'']
    ],
]);
NavBar::end();
?>
