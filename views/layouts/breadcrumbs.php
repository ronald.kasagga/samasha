<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 12:11 PM
 * @var $main_layout \yii\web\View
 */
use yii\widgets\Breadcrumbs; ?>

<?= Breadcrumbs::widget([
    'links' => isset($main_layout->params['breadcrumbs']) ? $main_layout->params['breadcrumbs'] : [],
]) ?>
