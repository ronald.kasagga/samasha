<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 12:16 PM
 */
use app\helper\NavHelper;
use app\helper\UserHelper;
use app\modules\donors\Module as DonorModule;
use app\modules\projects\Module as ProjectsModule;
use app\modules\commitments\Module as CommitmentsModule;
use yii\helpers\Url; ?>
<div class="sidebar" style="height: 1612px;">

    <div class="top">
        <a href="<?=Url::home()?>" class="logo"></a>
        <div class="search">
            <?=Yii::$app->controller->renderPartial('//layouts/_search')?>
        </div>
    </div>
    <div class="nContainer">
        <ul class="navigation bordered">
            <li class="<?=NavHelper::isHome() ? 'active' : ''?>"><a href="<?=Url::home()?>" class="blblue">
                    Uganda Commitment Initiative
                </a></li>
            <li class="<?=NavHelper::isActiveClass('donors')?>">
                <a href="<?=Url::toRoute('/donors')?>" class="blyellow">Partners</a>
            </li>
            <li class="<?=NavHelper::isActiveClass('donors')?>">
                <a href="<?=Url::toRoute('/reports/themes')?>" class="blorange">Theme Reports</a>
            </li>
            <?php if(UserHelper::isAdmin()):?>
            <li class="<?=NavHelper::isActiveController('currency')?>">
                <a href="<?=Url::to(['/currencies'])?>" class="blblue">Currency Manager</a>
            </li>
            <?php endif?>
            <li class="<?=NavHelper::isActiveController('initiatives')?>">
                <a href="<?=Url::to(['/commitments/initiatives'])?>" class="blgreen">Initiatives</a>
            </li>
            <?php if(UserHelper::isAdmin()):?>
            <li class="<?=NavHelper::isActiveClass('user')?>">
                <a href="#" class="blpurple">Users</a>
                <div class="open"></div>
                <?=NavHelper::generateSingleNavLevel(\app\models\AppUser::getAnchors())?>
            </li>
            <li class="<?=NavHelper::isActiveClass('logs')?>">
                <a href="<?=Url::toRoute('/logs')?>" class="blyellow">Logs</a>
            </li>
            <li class="<?=NavHelper::isActiveClass('feedback')?>">
                <a href="<?=Url::to(['/feedback/index'])?>" class="blblue">Feedback</a>
            </li>
            <?php endif?>
            <!--<li>
                <a href="#" class="blorange">Other</a>
                <div class="open"></div>
                <ul>
                    <li><a href="files.html">File handling</a></li>
                    <li><a href="images.html">Images</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="404.html">Error 404</a></li>
                </ul>
            </li>-->
        </ul>
        <a class="close">
            <span class="ico-remove"></span>
        </a>
    </div>
    <?php /*Yii::$app->controller->renderPartial('//layouts/_calendarWidget')*/?>

</div>
