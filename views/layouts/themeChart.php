<?
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/5/15
 * Time: 7:35 PM
 * @var $theme \app\models\Policy
 */
?>

<script type="application/javascript">
    $(function () {
        // Create the chart
        $('#chartData_<?=\app\helper\FormHelper::toMachineFriendlyString($theme->name)?>').highcharts({
            chart: {
                type: 'pie'
            },
            title: {
                text: '<?=$theme->name?>'
            },/*
             subtitle: {
             text: 'Click the slices to view versions. Source: netmarketshare.com.'
             },*/
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.idpos}'
                    }
                }
            },

            tooltip: {
                enabled:false,
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:#000">{point.name}</span><br/>'
            },
            series: [{
                name: "<?=$theme->name?> Indicators",
                colorByPoint: true,
                data: [
                    <?php $countIndicators = count($theme->indicators); $i = 0;?>
                    <?php foreach($theme->indicators as $indicator):?>
                    {
                        idpos: "<?=($i+1)?>",
                        name: "<?=$indicator->name?>",
                        link: "<?=yii\helpers\Url::to(['/reports/commodities/statistics', 'indicator'=>$indicator->id])?>",
                        y: <?=(1/$countIndicators) * 100?>,
                        drilldown: "<?=$indicator->name?>",
                        color: "<?=$indicator->statusHexColour?>"
                    }<?=($i++ <= $countIndicators)?',':''?>
                    <?php endforeach?>
                ],
                point: {
                    events:{
                        click: function (event) {
                            window.location = this.link;
                        }
                    }
                }
            }],
        });
    });
</script>
