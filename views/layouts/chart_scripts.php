<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/5/15
 * Time: 7:04 PM
 */
use app\models\Policy; ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<?php foreach(Policy::find()->all() as $theme):?>
    <?=Yii::$app->controller->renderPartial("//layouts/themeChart", ['theme'=>$theme])?>
<?php endforeach?>
