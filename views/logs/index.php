<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Log */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <!--<h1><?php/*= Html::encode($this->title) */?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-warning-sign"></span></div>
                    <h2>Logs</h2>
                </div>
                <div class="data-fluid">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'summary'=>'',
                        'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                        'rowOptions'=>  function ($model, $key, $index, $grid){
                            return \app\helper\FormHelper::tableRowOptions();
                        },
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'object_category',
                            //'object_id',
                            //'status',
                            'message',
                            'userName',
                            // 'detail:ntext',
                            'recorded_on',

                            //['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
