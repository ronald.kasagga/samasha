<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/3/15
 * Time: 5:52 PM
 * @var $logsDataProvider \yii\data\ActiveDataProvider
 */
use app\helper\UserHelper; ?>
<table width="100%" class="table tickets">
    <?php if(!empty($logsDataProvider->models)):?>
        <?php foreach($logsDataProvider->models as $log):?>
        <tr>
            <td class="bl_<?=$log->colour?>"><span class="label label-<?=$log->label?>"><?=$log->status?></span></td>
            <td style="min-width: 62px"><span class="mark"><?=$log->recorded_on?></span></td>
            <td>
                <a href="#" class="c<?=$log->colour?>"><?=$log->message?></a>
                <span class="mark">by <?=UserHelper::getUserFullName($log->user)?></span>
            </td>
        </tr>
        <?php endforeach?>
    <?php endif?>
</table>

