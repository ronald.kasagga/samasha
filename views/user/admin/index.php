<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<?php Pjax::begin() ?>

<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head blue">
                <div class="icon"><span class="ico-user"></span></div>
                <h2><?=$this->title?></h2>
                <span class="pull-right">
                    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Create User', ['create'], ['class' => 'btn']) ?>
                </span>
            </div>
            <div class="data-fluid">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel'  => $searchModel,
                    'layout'  => "{items}\n{pager}",
                    'summary'=>'',
                    'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                    'rowOptions'=>  function ($model, $key, $index, $grid){
                        return \app\helper\FormHelper::tableRowOptions();
                    },
                    'columns' => [
                        'username',
                        'email:email',
                        [
                            'attribute' => 'registration_ip',
                            'value' => function ($model) {
                                    return $model->registration_ip == null
                                        ? '<span class="not-set">' . Yii::t('user', '(not set)') . '</span>'
                                        : $model->registration_ip;
                                },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                            },
                            'filter' => DatePicker::widget([
                                'model'      => $searchModel,
                                'attribute'  => 'created_at',
                                'dateFormat' => 'php:Y-m-d',
                                'options' => [
                                    'class' => 'form-control'
                                ]
                            ]),
                        ],
                        [
                            'header' => Yii::t('user', 'Confirmation'),
                            'value' => function ($model) {
                                if ($model->isConfirmed) {
                                    return '<div class="text-center"><span class="text-success">' . Yii::t('user', 'Confirmed') . '</span></div>';
                                } else {
                                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-success btn-block',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                                    ]);
                                }
                            },
                            'format' => 'raw',
                            'visible' => Yii::$app->getModule('user')->enableConfirmation
                        ],
                        [
                            'header' => Yii::t('user', 'Admin'),
                            'format'=>'raw',
                            'value' => function($model){
                                return \app\helper\UserHelper::adminStatusButton($model);
                            }
                        ],
                        [
                            'header' => Yii::t('user', 'Block status'),
                            'value' => function ($model) {
                                if ($model->isBlocked) {
                                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-success btn-block',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?')
                                    ]);
                                } else {
                                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-danger btn-block',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?')
                                    ]);
                                }
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'contentOptions'=>['style'=>'width:100px'],
                            'buttons' =>[
                                'update'=>function($url, $model){
                                    return Html::a('<div class="icon"><span class="ico-pencil-2"></span></div>', $url, ['class'=>'button blue']);
                                },
                                'delete'=>function($url, $model){
                                    return Html::a('<div class="icon"><span class="ico-trash"></span></div>', $url, ['class'=>'button red']);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php Pjax::end() ?>
