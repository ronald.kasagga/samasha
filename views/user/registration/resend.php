<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/27/15
 * Time: 1:39 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\resendForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = Yii::t('user', 'Request new confirmation message');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="resend" id="samasha-login-form">
    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

    <div class="page-header">
        <div class="icon">
            <span class="ico-arrow-right"></span>
        </div>
        <h1><?= Html::encode($this->title) ?> <small><?=Yii::$app->name?></small></h1>
    </div>

    <div style="float: left; width:350px">

        <?php $form = ActiveForm::begin([
            'id'                     => 'password-resend-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'options'=>['class'=>'col-md-offset-1']
        ]); ?>

        <div class="row-form">
            <div class="col-md-12 field-resend-form-email required">
                <?= Html::activeInput('email', $model, 'email', ['id'=>'resend-form-email','class' => 'form-control', 'tabindex' => '2', 'placeholder'=>'email']) ?>
                <div class="help-block">
                    <?=isset($model->errors['email'])?$model->errors['email'][0]:''?>
                </div>
            </div>
        </div>

        <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'btn btn-primary btn-block']) ?><br>

        <?php ActiveForm::end(); ?>
        <p class="text-center">
            <?= Html::a('<span class="ico-arrow-left"></span> Home', Url::home(), ['style'=>'font-weight:bold']) ?>
        </p>
    </div>
</div>

