<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/27/15
 * Time: 1:39 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;

/**
* @var yii\web\View                   $this
* @var dektrium\user\models\LoginForm $model
* @var dektrium\user\Module           $module
*/

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login" id="samasha-login-form">
    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

    <div class="page-header">
        <div class="icon">
            <span class="ico-arrow-right"></span>
        </div>
        <h1><?= Html::encode($this->title) ?> <small><?=Yii::$app->name?></small></h1>
    </div>

    <div style="float: left;">

        <?php $form = ActiveForm::begin([
            'id'                     => 'login-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'validateOnBlur'         => false,
            'validateOnType'         => false,
            'validateOnChange'       => false,
        ]) ?>

        <div class="row-form">
            <div class="col-md-12 field-login-form-login required">
                <?=Html::activeInput('text', $model, 'login', ['id'=>'login-form-login','autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1', 'placeholder'=>'username'])?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="row-form">
            <div class="col-md-12 field-login-form-password required">
                <?= Html::activeInput('password', $model, 'password', ['id'=>'login-form-password','class' => 'form-control', 'tabindex' => '2', 'placeholder'=>'password']) ?>
                <div class="help-block">
                    <?=isset($model->errors['password'])?$model->errors['password'][0]:''?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="col-md-12">
                <?= Html::activeCheckbox($model, 'rememberMe', ['tabindex' => '4']) ?>
            </div>
        </div>
        <div class="row-form">
            <div class="col-md-12">
                <?= Html::submitButton(Yii::t('user', 'Sign in').'<span class="icon-arrow-next icon-white"></span>',
                    ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <?php if($module->enablePasswordRecovery):?>
            <p class="text-center">
                <?=Html::a(Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5'])?>
            </p>
        <?php endif?>

        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth']
        ]) ?>
        <p class="text-center">
            <?= Html::a('<span class="ico-arrow-left"></span> Home', Url::home(), ['style'=>'font-weight:bold']) ?>
        </p>
    </div>
</div>

