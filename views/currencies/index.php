<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Currency */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-md-7">
        <div class="row-fluid">
            <div class="span12">
                <div class="block">
                    <div class="head blue">
                        <div class="icon"><span class="ico-coins"></span></div>
                        <h2>Currencies</h2>
                    </div>
                    <div class="data-fluid">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'summary'=>'',
                            'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                            'rowOptions'=>  function ($model, $key, $index, $grid){
                                return \app\helper\FormHelper::tableRowOptions();
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'code',
                                'description:ntext',
                                'ugx_exchange',

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'contentOptions'=>['style'=>'width:140px'],
                                    'template'=>'{view} {update} {delete}',
                                    'buttons'=>[
                                        'view'=>function($url, $model){
                                            return Html::a('<div class="icon"><span class="ico-eye-open"></span></div>', $url, ['class'=>'button green']);
                                        },
                                        'update'=>function($url, $model){
                                            return Html::a('<div class="icon"><span class="ico-pencil-2"></span></div>', $url, ['class'=>'button blue']);
                                        },
                                        'delete'=>function($url, $model){
                                            return Html::a('<div class="icon"><span class="ico-trash"></span></div>', $url, ['class'=>'button red']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <h3>Add Currency</h3>
        <?php $model = new\app\models\Currency()?>
        <?= $this->render('_form', [
            'model' => $model,
            'config'=>['action'=>Url::toRoute('/currencies/create')]
        ]) ?>
    </div>

</div>
