<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/23/2015
 * Time: 10:36 AM
 * @var \app\models\Policy $theme
 */?>

<div class="row">
    <div class="col-md-6"
         id="chartData_<?=\app\helper\FormHelper::toMachineFriendlyString($theme->name)?>"
         style="height: 300px; margin: 0 auto">
    </div>
    <div class="col-md-6">
        <h4 class=""><?=$theme->name?> Indicators</h4>
        <?php if(!empty($theme->indicators)):?>
            <ol>
                <?php foreach($theme->indicators as $indicator):?>
                    <li class="indicator-label"><?=$indicator->name?></li>
                <?php endforeach?>
            </ol>
        <?php endif?>
    </div>
</div>
