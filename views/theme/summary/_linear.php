<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/23/2015
 * Time: 10:36 AM
 * @var \app\models\Policy $theme
 */
use app\helper\StringHelper;
use yii\helpers\Url; ?>

<div class="block">
    <div class="head">
        <h3>
            <i class="icon"><span class="<?=$theme->icon?>"></span></i>
            <?=$theme->name?>
        </h3>
    </div>
    <div class="data-fluid">
        <?php if(!empty($theme->indicators)):?>
        <?php list($currentYear, $nextYear) = explode('/', \app\models\Indicator::getCurrentFiscalYear())?>
        <script type="text/javascript">
            var fiscalData = <?= json_encode(['Achievement[fiscalYear]'=>$currentYear])?>;
        </script>
        <table class="table table-hover tickets">
            <?php foreach($theme->indicators as $indicator):?>
                <?php $url = Url::to(['/reports/commodities/statistics', 'indicator'=>$indicator->id])?>
                <tr style="cursor:pointer"
                    onclick="postLink('<?=$url?>', fiscalData)">
                    <td class="col-md-11" style="border-left: 15px solid <?=$indicator->statusHexColour?>;">
                        <?=StringHelper::formatStringWithBreaks($indicator->name)?>
                    </td>
                    <td class="col-md-1"><?=$indicator->initiative->name?></td>
                </tr>
            <?php endforeach?>
        </table>
        <?php endif?>
    </div>
</div>
