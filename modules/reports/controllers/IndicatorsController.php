<?php

namespace app\modules\reports\controllers;

use app\models\Initiative;
use app\models\Policy;
use Yii;
use app\models\Indicator;
use app\modules\reports\models\Indicator as IndicatorSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IndicatorsController implements the CRUD actions for Indicator model.
 */
class IndicatorsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Indicator models.
     * @param $theme
     * @return mixed
     */
    public function actionIndex($theme)
    {
        $searchModel = new IndicatorSearch();
        $params = Yii::$app->request->queryParams;
        $params['Indicator']['theme_id'] = $theme;
        $dataProvider = $searchModel->search($params);
        //Indicator::setCurrentYear(date('Y')-1);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'theme'=>Policy::findOne($theme),
        ], $this->formData()));
    }

    public function actionReport($theme)
    {
        $searchModel = new IndicatorSearch();
        $params = Yii::$app->request->queryParams;
        $params['Indicator']['theme_id'] = $theme;
        $dataProvider = $searchModel->search($params);

        return $this->render('report', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'theme'=>Policy::findOne($theme),
            'themes'=>ArrayHelper::map(Policy::find()->all(), 'id', 'name'),
        ], $this->formData()));
    }

    /**
     * Displays a single Indicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
        ], $this->formData()));
    }

    /**
     * Creates a new Indicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $theme
     * @return mixed
     */
    public function actionCreate($theme)
    {
        $model = new Indicator();

        if ($model->load(Yii::$app->request->post())) {
            $model->theme_id = $theme;
            if($model->save())
                return $this->redirect(['index', 'theme'=>$model->theme_id]);
        }
        return $this->render('create', ArrayHelper::merge([
            'model' => $model,
            'theme' => Policy::findOne($theme),
        ], $this->formData()));
    }

    /**
     * Updates an existing Indicator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'theme'=>$model->theme_id]);
        } else {
            return $this->render('update', ArrayHelper::merge([
                'model' => $model,
                'theme' => Policy::findOne($model->theme_id),
            ], $this->formData()));
        }
    }

    /**
     * Deletes an existing Indicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData()
    {
        return [
            'initiatives'=>ArrayHelper::map(Initiative::find()->all(), 'id', 'name')
        ];
    }
}
