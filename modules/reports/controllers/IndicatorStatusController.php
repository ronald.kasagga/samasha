<?php

namespace app\modules\reports\controllers;

use app\helper\FormHelper;
use app\models\Indicator;
use Yii;
use app\models\IndicatorStatus;
use app\modules\reports\models\IndicatorStatus as IndicatorStatusSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IndicatorStatusController implements the CRUD actions for IndicatorStatus model.
 */
class IndicatorStatusController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IndicatorStatus models.
     * @param $indicator
     * @return mixed
     */
    public function actionIndex($indicator)
    {
        $searchModel = new IndicatorStatusSearch();
        $params = Yii::$app->request->queryParams;
        $params['IndicatorStatus']['indicator_id'] = $indicator;
        $dataProvider = $searchModel->search($params);
        $indicator = Indicator::findOne($indicator);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'indicator' => $indicator,
        ], $this->formData()));
    }

    public function actionViewStatus($indicator)
    {
        //Indicator::setCurrentYear(date('Y')-1);
        $searchModel = new IndicatorStatusSearch();
        $params = Yii::$app->request->queryParams;
        $params['IndicatorStatus']['indicator_id'] = $indicator;
        //$params['IndicatorStatus']['fiscal_year'] = Indicator::getCurrentFiscalYear();
        $dataProvider = $searchModel->search($params);
        $indicator = Indicator::findOne($indicator);

        return $this->render('view-status', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'indicator' => $indicator,
        ], $this->formData()));
    }

    /**
     * Displays a single IndicatorStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
        ], $this->formData()));
    }

    /**
     * Creates a new IndicatorStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $indicator
     * @return mixed
     */
    public function actionCreate($indicator)
    {
        $model = new IndicatorStatus();

        if ($model->load(Yii::$app->request->post())) {
            $model->indicator_id = $indicator;
            if($model->save())
                return $this->redirect(['index', 'indicator' => $model->indicator_id]);
        }
        return $this->render('create', ArrayHelper::merge([
            'model' => $model,
        ], $this->formData()));
    }

    /**
     * Updates an existing IndicatorStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $indicator = $model->indicator_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->indicator_id = $indicator;
            if($model->save())
                return $this->redirect(['index', 'indicator' => $model->indicator_id]);
        }
        return $this->render('update', ArrayHelper::merge([
            'model' => $model,
            'indicator' => Indicator::findOne($indicator),
        ], $this->formData()));
    }

    /**
     * Deletes an existing IndicatorStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $indicator_id = $model->indicator_id;
        $model->delete();

        return $this->redirect(['index', 'indicator'=>$indicator_id]);
    }

    /**
     * Finds the IndicatorStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IndicatorStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IndicatorStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData($fiscalYearsOmitted = [])
    {
        return [
            'statuses'=>FormHelper::toAssociativeArray(IndicatorStatus::getStatusOptions()),
            'years'=>FormHelper::toAssociativeArray(IndicatorStatus::getFiscalYearOptions($fiscalYearsOmitted)),
        ];
    }
}
