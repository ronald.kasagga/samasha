<?php

namespace app\modules\reports\controllers;

use app\models\Contribution;
use app\models\Currency;
use app\models\Donor;
use app\models\Indicator;
use app\modules\donors\models\IndicatorFund;
use Yii;
use app\models\Commodity;
use app\modules\reports\models\Commodity as CommoditySearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommoditiesController implements the CRUD actions for Commodity model.
 */
class CommoditiesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Commodity models.
     * @param $indicator
     * @return mixed
     */
    public function actionIndex($indicator)
    {
        $searchModel = new CommoditySearch();
        $params = Yii::$app->request->queryParams;
        $params['Commodity']['indicator_id'] = $indicator;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'indicator' => Indicator::findOne($indicator),
        ], $this->formData()));
    }

    public function actionPartner($id, $indicator)
    {
        $fundSearchModel = new IndicatorFund();
        $contributionSearchModel = new \app\modules\donors\models\Contribution();
        $fundDataProvider = $fundSearchModel->agencyForIndicator(Yii::$app->request->queryParams, $id, $indicator);
        $contributionDataProvider = $contributionSearchModel->agencyForIndicator(Yii::$app->request->queryParams, $id, $indicator);

        return $this->render('partner', ArrayHelper::merge([
            'indicator' => Indicator::findOne($indicator),
            'model' => Donor::findOne($id),
            'fundingSearchModel' => $fundSearchModel,
            'contributionSearchModel' => $contributionSearchModel,
            'fundingDataProvider' => $fundDataProvider,
            'contributionDataProvider' => $contributionDataProvider,
        ], $this->formData()));
    }

    public function actionStatistics($indicator)
    {
        $partners = Donor::getActivePartners($indicator);

        return $this->render('statistics', ArrayHelper::merge([
            'partners' => $partners,
            'indicator' => Indicator::findOne($indicator),
            'model' => new Commodity(),
        ], $this->formData()));
    }

    /**
     * Displays a single Commodity model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'indicator' => Indicator::findOne($model->indicator_id),
        ]);
    }

    /**
     * Creates a new Commodity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $indicator
     * @return mixed
     */
    public function actionCreate($indicator)
    {
        $model = new Commodity();

        if ($model->load(Yii::$app->request->post())) {
            $model->indicator_id = $indicator;
            if($model->save())
                return $this->redirect(['index', 'indicator' => $model->indicator_id]);
        }
        return $this->render('create', ArrayHelper::merge([
            'model' => $model,
            'indicator' => Indicator::findOne($indicator),
        ], $this->formData()));
    }

    /**
     * Updates an existing Commodity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $indicator = $model->indicator_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->indicator_id = $indicator;
            if($model->save())
                return $this->redirect(['index', 'indicator' => $model->indicator_id]);
        }
        return $this->render('update', ArrayHelper::merge([
            'model' => $model,
            'indicator' => Indicator::findOne($indicator),
        ], $this->formData()));
    }

    /**
     * Deletes an existing Commodity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Commodity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Commodity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Commodity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData()
    {
        return [
            'agencies' => ArrayHelper::map(Donor::find()->all(), 'id', 'name'),
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'code', 'description'),
        ];
    }
}
