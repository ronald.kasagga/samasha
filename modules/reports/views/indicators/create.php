<?php



/* @var $this yii\web\View */
/* @var $model app\models\Indicator */
/* @var $initiatives array */
/* @var $theme \app\models\Policy */

$this->title = 'Create Indicator (Theme: '. $theme->name .')';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-create">

    <?= $this->render('_form', [
        'model' => $model,
        'initiatives' => $initiatives,
    ]) ?>

</div>
