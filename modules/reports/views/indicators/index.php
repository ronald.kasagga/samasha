<?php

use app\helper\UserHelper;
use app\models\Indicator;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\Indicator */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $theme \app\models\Policy */
/* @var $initiatives array */

$this->title = $theme->name. ' Indicators';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = $this->title . ' (' . Indicator::getCurrentFiscalYear() . ')';
?>
<div class="indicator-index">

    <?php
    $columns = [
        ['class' => 'yii\grid\SerialColumn'],

        ['attribute'=>'name', 'contentOptions'=>['style'=>'width:300px']],
        ['attribute'=>'initiative_id', 'value'=>'initiative.name', 'filter'=>$initiatives],
        ['label'=>'Commodities', 'value'=>'numberOfCommodities',],
        ['label'=>'Partners', 'value'=>'listFundingAgencies', 'contentOptions'=>['style'=>'width:200px']],
        ['label'=>'Status', 'value'=>'status', 'format'=>'raw',],
    ];
    $actions = [
        'class' => 'yii\grid\ActionColumn',
        'header'=>UserHelper::isAdmin()?'Actions':'Barriers',
        'options'=>['style'=>UserHelper::isAdmin()?'width:180px':''],
        'template'=>UserHelper::isAdmin()?'{update_status} {manage_commodities} {view_report} {update}':'{view_status}',
        'buttons'=>[
            'view_status'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-eye-open"></span></div>', ['/reports/indicator-status/view-status', 'indicator'=>$model->id], ['class'=>'button green', 'title'=>'View Barriers']);
            },
            'update_status'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-tools"></span></div>', ['/reports/indicator-status/index', 'indicator'=>$model->id], ['class'=>'button yellow', 'title'=>'Update Status']);
            },
            'manage_commodities'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-tasks"></span></div>', ['/reports/commodities/statistics', 'indicator'=>$model->id], ['class'=>'button blue', 'title'=>'Contributions']);
            },
            'view_report'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-folder-open"></span></div>', ['/reports/indicators/report', 'theme'=>$model->id], ['class'=>'button green', 'title'=>'View Report']);
            },
            'update'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-edit"></span></div>', ['update', 'id'=>$model->id], ['class'=>'button purple', 'title'=>'Edit']);
            },
        ]
    ];

    $columns[] = $actions?>

    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-stack-2"></span></div>
                    <h2><?=$this->title?></h2>
                    <?php if(UserHelper::isAdmin()):?>
                        <span class="pull-right">
                            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Create Indicator', ['create', 'theme'=>$theme->id], ['class' => 'btn']) ?>
                        </span>
                    <?php endif?>
                </div>
                <div class="data-fluid">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'summary'=>'',
                        'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                        'rowOptions'=>  function ($model, $key, $index, $grid){
                            return \app\helper\FormHelper::tableRowOptions();
                        },
                        'columns' => $columns,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
