<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Indicator */
/* @var $initiatives array */
/* @var $theme \app\models\Policy */

$this->title = 'Update Indicator (Under Theme: '. $theme->name .')';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$theme->id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="indicator-update">


    <?= $this->render('_form', [
        'model' => $model,
        'initiatives' => $initiatives,
    ]) ?>

</div>
