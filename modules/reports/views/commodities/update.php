<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Commodity */

$this->title = 'Update Commodity: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commodity-update">

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'agencies' => $agencies,
        'currencies' => $currencies,
    ]) ?>

</div>
