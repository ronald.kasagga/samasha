<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Commodity */

$this->title = 'Create Commodity';
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodity-create">

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'agencies' => $agencies,
        'currencies' => $currencies,
    ]) ?>

</div>
