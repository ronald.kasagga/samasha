<?php

use app\helper\TimeHelper;
use app\models\Indicator;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\Commodity */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Donor */
/* @var $indicator Indicator */
/* @var $partners \app\models\Donor[] */

$this->title = 'Indicator Partners ('.Indicator::getCurrentFiscalYear().')';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodity-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>

    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-money-bag"></span></div>
                    <h2><?=$this->title?></h2>
                </div>
                <div class="data-fluid">
                    <table class="table table-responsive">
        <thead>
            <tr>
                <th class="col-md-2"><?=$model->getAttributeLabel('agency_id')?></th>
                <th class="col-md-2">Funding</th>
                <th class="col-md-8">Contributions</th>
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($partners)):?>
            <?php foreach($partners as $partner):?>
                <tr>
                    <td><?=Html::a($partner->name, ['partner', 'id'=>$partner->id, 'indicator'=>$indicator->id])?></td>
                    <td>
                        <?php $partnerFund = $partner->getFunding($indicator->id)?>
                        <?php if(!empty($partnerFund)){?>
                            <ul>
                                <?php foreach($partnerFund as $funding):?>
                                    <li><?=$funding->getAmountWithCurrency() . ' on ' . TimeHelper::formatDate($funding->added_on)?></li>
                                <?php endforeach?>
                            </ul>
                        <?php } else echo  'None'?>
                    </td>
                    <td>
                        <?php $contributions = $partner->getContributions($indicator->id)?>
                        <?php if(!empty($contributions)){?>
                        <ol>
                            <?php foreach($contributions as $contribution):?>
                                <li>
                                    <p>
                                        <?=$contribution->name.'<br>'
                                        .str_replace(["\r\n", "\n\r", "\r", "\n"], '<br>', $contribution->description)?>
                                    </p>
                                </li>
                            <?php endforeach?>
                        </ol>
                        <?php }else echo 'None'?>
                    </td>
                </tr>
            <?php endforeach?>
        <?php endif?>
        </tbody>
    </table>
                </div>
            </div>
        </div>
    </div>

</div>
