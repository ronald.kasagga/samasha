<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\reports\models\Commodity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commodity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'indicator_id') ?>

    <?= $form->field($model, 'agency_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'expenditure') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'expenditure_ugx') ?>

    <?php // echo $form->field($model, 'deposited_on') ?>

    <?php // echo $form->field($model, 'recorded_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
