<?php

use app\models\Commodity;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\Commodity */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contributions';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodity-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            ['attribute'=>'agency_id', 'value'=>'agency.name'],
            ['attribute'=>'description', 'value'=>function(Commodity $data){
                return \yii\helpers\StringHelper::truncateWords($data->description, 10);
            }],
            ['attribute'=>'expenditure', 'value'=>function(Commodity $data){
                return $data->currency.' '.number_format($data->expenditure);
            }],
            ['attribute'=>'expenditure_ugx', 'value'=>function(Commodity $data){
                return 'UGX '.number_format($data->expenditure_ugx);
            }],
            'deposited_on',
            // 'recorded_on',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons'=>[
                    'view'=>function($url, $model){
                        return Html::a('<div class="icon"><span class="ico-eye-open"></span></div>', ['view', 'id'=>$model->id], ['class'=>'button yellow', 'title'=>'Update Status']);
                    },
                ]
            ],
        ],
    ]); ?>

</div>
