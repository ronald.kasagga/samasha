<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Commodity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            ['attribute'=>'indicator_id', 'value'=>$model->indicator->theme->name.': '.$model->indicator->description],
            ['attribute'=>'agency_id', 'value'=>$model->agency->name],
            'description:ntext',
            ['attribute'=>'expenditure', 'value'=>$model->currency.' '.number_format($model->expenditure)],
            ['attribute'=>'expenditure_ugx', 'value'=>'UGX .'.number_format($model->expenditure_ugx)],
            'deposited_on',
            'recorded_on',
        ],
    ]) ?>

</div>
