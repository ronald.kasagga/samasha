<?php

use app\helper\UserHelper;
use app\models\IndicatorFund;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Donor */
/* @var $indicator app\models\Indicator */
/* @var $approval_statuses array */
/* @var $fundingSearchModel app\modules\donors\models\IndicatorFund */
/* @var $fundingDataProvider yii\data\ActiveDataProvider */
/* @var $contributionSearchModel app\modules\donors\models\Contribution */
/* @var $contributionDataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-view">

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>

    <div class="">
        <div class="row col-md-6">
            <div class="row-fluid">
                <div class="span12">
                    <div class="block">
                        <div class="head blue">
                            <div class="icon"><span class="ico-coins"></span></div>
                            <h2>Indicator Funding</h2>
                        </div>
                        <div class="data-fluid">
                            <?= GridView::widget([
                                'dataProvider' => $fundingDataProvider,
                                'summary'=>'',
                                'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                                'rowOptions'=>  function ($model, $key, $index, $grid){
                                    return \app\helper\FormHelper::tableRowOptions();
                                },
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    ['attribute'=>'contribution_amount', 'value'=>'amountWithCurrency', 'label'=>'Funding Amount'],
                                    ['attribute'=>'amount_ugx', 'value'=>function(IndicatorFund $data){
                                        return number_format($data->amount_ugx);}
                                    ],
                                    'added_on',

                                    //['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-6">
            <div class="row-fluid">
                <div class="span12">
                    <div class="block">
                        <div class="head blue">
                            <div class="icon"><span class="ico-stack-2"></span></div>
                            <h2>Indicator Contributions</h2>
                        </div>
                        <div class="data-fluid">
                            <?= GridView::widget([
                'dataProvider' => $contributionDataProvider,
                'summary'=>'',
                'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                'rowOptions'=>  function ($model, $key, $index, $grid){
                    return \app\helper\FormHelper::tableRowOptions();
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    ['attribute'=>'name', 'label'=>'Contribution'],
                    ['attribute'=>'description',],
                    // 'added_on',

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
