<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\Indicator */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $theme \app\models\Policy */
/* @var $initiatives array */

$this->title = $theme->name. ' Report';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = $this->title . ' (' .\app\models\Indicator::getCurrentFiscalYear().')';;
?>
<div class="indicator-index">

    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-money-bag"></span></div>
                    <h2><?=$this->title?></h2>
                </div>
                <div class="data-fluid">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'summary'=>'',
                        'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                        'rowOptions'=>  function ($model, $key, $index, $grid){
                            return \app\helper\FormHelper::tableRowOptions();
                        },
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            ['attribute'=>'initiative_id', 'value'=>'initiative.name'],
                            ['attribute'=>'theme_id', 'value'=>'theme.name'],
                            ['attribute'=>'name', 'label'=>'Indicator'],
                            ['label'=>'Partners', 'value'=>'financialSourcesCount',],
                            ['label'=>'Status', 'value'=>'latestStatus', 'format'=>'raw'],
                            ['label'=>'Status Date ', 'value'=>'latestStatusDate'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
