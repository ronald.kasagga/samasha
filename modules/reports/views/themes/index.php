<?php

use app\helper\UserHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\Theme */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Theme Reports';
$this->params['breadcrumbs'][] = $this->title . ' (' .\app\models\Indicator::getCurrentFiscalYear().')';
?>
<div class="policy-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-folder-open"></span></div>
                    <h2><?=$this->title?></h2>
                </div>
                <div class="data-fluid">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'summary'=>'',
                        'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                        'rowOptions'=>  function ($model, $key, $index, $grid){
                            return \app\helper\FormHelper::tableRowOptions();
                        },
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'name',
                            ['label'=>'Indicators', 'value'=>'numberOfIndicators'],
                            ['label'=>'Partners', 'value'=>'numberOfFundingSources'],
                            ['label'=>'Status', 'value'=>'colouredStatus', 'format'=>'raw'],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template'=>'{indicators} {view_report} {donors}',
                                'buttons'=>[
                                    'indicators'=>function($url, $model){
                                        return Html::a('<div class="icon"><span class="ico-pushpin"></span></div>', ['/reports/indicators/index', 'theme'=>$model->id], ['class'=>'button purple', 'title'=>'Indicator Manager']);
                                    },
                                    'donors'=>function($url, $model){
                                        return Html::a('<div class="icon"><span class="ico-money-bag"></span></div>', ['/reports/themes/donors', 'theme'=>$model->id], ['class'=>'button green', 'title'=>'View Donors']);
                                    },
                                    'view_report'=>function($url, $model){
                                        return Html::a('<div class="icon"><span class="ico-folder-open"></span></div>', ['/reports/themes/report', 'id'=>$model->id], ['class'=>'button orange', 'title'=>'View Report']);
                                    },
                                ]

                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
