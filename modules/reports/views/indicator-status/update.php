<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\IndicatorStatus */
/* @var $indicator app\models\Indicator */

$this->title = 'Update Indicator Status ';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($model->indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => 'Indicator Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update '.$model->description;
?>
<div class="indicator-status-update">

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
        'years' => $years,
    ]) ?>

</div>
