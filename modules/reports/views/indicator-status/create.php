<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IndicatorStatus */

$this->title = 'Create Indicator Status';
$this->params['breadcrumbs'][] = ['label' => 'Indicator Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
        'years' => $years,
    ]) ?>

</div>
