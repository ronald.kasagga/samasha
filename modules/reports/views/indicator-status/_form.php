<?php

use app\models\Indicator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndicatorStatus */
/* @var $form yii\widgets\ActiveForm */
$config = isset($config) ? $config : [];
?>

<div class="indicator-status-form">

    <?php $form = ActiveForm::begin($config); ?>

    <div class="row form-group field-indicatorstatus-status required has-success">
        <label class="control-label" for="indicatorstatus-status"><?=$model->getAttributeLabel('status')?></label>
        <?=Html::activeDropDownList($model, 'status', $statuses, ['class'=>'select col-lg-12'])?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'description')->textArea(['rows' => 3]) ?>

    <div class="form-group field-indicatorstatus-fiscal_year required has-success">
        <label class="control-label" for="indicatorstatus-fiscal_year">Fiscal Year</label>
        <?=Html::activeDropDownList($model, 'fiscal_year', $years, ['class'=>'select col-lg-12', 'selection'=>Indicator::getCurrentFiscalYear()])?>
        <div class="help-block"></div>
    </div>
    <br><br><br>

    <div class="form-group row col-md-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
