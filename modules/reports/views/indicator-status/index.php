<?php

use app\models\Indicator;
use app\models\IndicatorStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reports\models\IndicatorStatus */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $indicator \app\models\Indicator */

$this->title = 'Indicator Statuses ';
$this->params['breadcrumbs'][] = ['label'=>'Theme Reports', 'url'=>['/reports/themes']];
$this->params['breadcrumbs'][] = ['label'=>$indicator->theme->name. ' Indicators', 'url'=>['/reports/indicators/index', 'theme'=>$indicator->theme_id]];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($indicator->name, 13), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-status-index">

    <h3><?= Html::encode($indicator->theme->name .' Indicator: ' . $indicator->description) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-md-8">
        <div class="row-fluid">
            <div class="span12">
                <div class="block">
                    <div class="head blue">
                        <div class="icon"><span class="ico-stats"></span></div>
                        <h2><?=$this->title?></h2>
                    </div>
                    <div class="data-fluid">
                        <?php $statusColumns = [
                            ['class' => 'yii\grid\SerialColumn'],

                            ['attribute'=>'status', 'format'=>'raw', 'value'=>function(IndicatorStatus $data){
                                return Html::a(Indicator::getStatusBadge($data->status), ['update', 'id'=>$data->id]);
                            }],
                            'description',
                            'fiscal_year',
                            // 'recorded_on',
                        ];

                        if(\app\helper\UserHelper::isAdmin())
                            $statusColumns[] = [
                                'class' => 'yii\grid\ActionColumn',
                                'template'=>'{delete}',
                                'buttons'=>[
                                    'delete'=>function($url, $model){
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                                            [
                                                'style'=>'color:red',
                                                'data' => [
                                                    'confirm' => 'Are you sure you want to delete this status?',
                                                    'method' => 'post',
                                                ],
                                            ]
                                        );
                                    },
                                ]
                            ]?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'summary'=>'',
                            'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                            'rowOptions'=>  function ($model, $key, $index, $grid){
                                return ArrayHelper::merge(
                                    \app\helper\FormHelper::tableRowOptions(),
                                    ['id' => 'record#'.$model['id'], 'class'=>IndicatorStatus::getStatusCode($model['status'])]);
                            },
                            'columns' => $statusColumns,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="row-fluid">
            <div class="span12">
                <div class="block">
                    <div class="head blue">
                        <div class="icon"><span class="ico-plus-sign"></span></div>
                        <h2>Record Status</h2>
                    </div>
                    <div class="data-fluid">
                        <?php $model = new IndicatorStatus() ?>
                        <?= $this->render('_form', [
                            'model' => $model,
                            'statuses' => $statuses,
                            'years' => $years,
                            'config'=>['action'=>Url::toRoute(['create', 'indicator'=>$indicator->id])]
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>
