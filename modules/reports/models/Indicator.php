<?php

namespace app\modules\reports\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Indicator as IndicatorModel;

/**
 * Indicator represents the model behind the search form about `app\models\Indicator`.
 */
class Indicator extends IndicatorModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'theme_id', 'initiative_id'], 'integer'],
            [['recorded_on', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IndicatorModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'recorded_on' => $this->recorded_on,
            'theme_id' => $this->theme_id,
            'initiative_id' => $this->initiative_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
