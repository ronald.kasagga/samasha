<?php

namespace app\modules\reports\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Commodity as CommodityModel;

/**
 * Commodity represents the model behind the search form about `app\models\Commodity`.
 */
class Commodity extends CommodityModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'indicator_id', 'agency_id'], 'integer'],
            [['name', 'description', 'currency', 'deposited_on', 'recorded_on'], 'safe'],
            [['expenditure', 'expenditure_ugx'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommodityModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $this->indicator_id,
            'agency_id' => $this->agency_id,
            'expenditure' => $this->expenditure,
            'expenditure_ugx' => $this->expenditure_ugx,
            'deposited_on' => $this->deposited_on,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
