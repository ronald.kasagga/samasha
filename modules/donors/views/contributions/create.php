<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contribution */
/* @var $indicators array */

$this->title = 'Add Indicator Contribution';
$this->params['breadcrumbs'][] = ['label' => 'Contribution Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $agency->name, 'url' => ['/donors/donor/view', 'id'=>$agency->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Contributions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contribution-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'indicators' => $indicators,
    ]) ?>

</div>
