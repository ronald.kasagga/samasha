<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contribution */
/* @var $form yii\widgets\ActiveForm */
/* @var $indicators array */
$model->contributed_on = ($model->contributed_on != null ? $model->contributed_on : date('Y-m-d'))
?>

<div class="contribution-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row form-group field-contribution-indicator_id" style="margin-left: -15px; margin-right: -15px">
        <label class="control-label col-lg-12" for="contribution-indicator_id" >
            <?=$model->getAttributeLabel('indicator_id')?>
        </label>
        <?=Html::activeDropDownList($model, 'indicator_id', $indicators,
            ['prompt'=>'Choose Indicator...', 'class'=>'select col-lg-12'])?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group field-contribution-contributed_on col-lg-4 required" style="margin-left: -15px; margin-right: -15px">
        <label class="control-label" for="contribution-contributed_on">Contributed On</label>
        <?=Html::activeInput('text', $model, 'contributed_on', ['class'=>'form-control datepicker'])?>
        <div class="help-block"></div>
    </div>

    <br><br><br><br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary pull-left']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
