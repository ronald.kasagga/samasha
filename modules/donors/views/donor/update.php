<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Donor */
/* @var $approval_statuses array */

$this->title = 'Update Agency: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="donor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'approval_statuses' => $approval_statuses,
    ]) ?>

</div>
