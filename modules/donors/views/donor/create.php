<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Donor */
/* @var $approval_statuses array */

$this->title = 'Create Partner';
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'approval_statuses' => $approval_statuses,
    ]) ?>

</div>
