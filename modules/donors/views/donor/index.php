<?php

use app\helper\UserHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\donors\models\Donor */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $approval_statuses array */

$this->title = 'Partners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $columns = [
        ['class' => 'yii\grid\SerialColumn'],

        ['attribute'=>'name', 'value'=>'donorLink', 'format'=>'raw'],
        ['attribute'=>'description', 'value'=>'truncatedDescription'],
    ];
    if(UserHelper::isAdmin()){
        $columns[] = 'contact_person';
        $columns[] = 'contact_email:email';
        $columns[] = 'contact_phone';
        /*['attribute'=>'is_approved', 'value'=>'verboseApproval',
            'filter'=>Html::activeDropDownList($searchModel, 'is_approved', $approval_statuses, ['class'=>'form-control', 'prompt'=>'Choose ...'])
        ],*/

        $columns[] = [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions'=>['style'=>'width:140px'],
            'template'=>'{view} {update} {delete}',
            'buttons'=>[
                'view'=>function($url, $model){
                    return Html::a('<div class="icon"><span class="ico-eye-open"></span></div>', $url, ['class'=>'button green']);
                },
                'update'=>function($url, $model){
                    return Html::a('<div class="icon"><span class="ico-pencil-2"></span></div>', $url, ['class'=>'button blue']);
                },
                'delete'=>function($url, $model){
                    return Html::a('<div class="icon"><span class="ico-trash"></span></div>', $url,
                        [
                            'class'=>'button red',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this partner? Data of their contributions will also be lost.',
                                'method' => 'post',
                            ],
                        ]);
                },
            ]
        ];
    } ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-open"></span></div>
                    <h2><?=$this->title?></h2>
                    <?php if(UserHelper::isAdmin()):?>
                        <span class="pull-right">
                            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Create Partner', ['create'], ['class' => 'btn']) ?>
                        </span>
                    <?php endif?>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'summary'=>'',
                    'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                    'rowOptions'=>  function ($model, $key, $index, $grid){
                        return \app\helper\FormHelper::tableRowOptions();
                    },
                ]); ?>
            </div>
        </div>
    </div>

</div>
