<?php

use app\helper\UserHelper;
use app\models\IndicatorFund;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Donor */
/* @var $approval_statuses array */
/* @var $fundingSearchModel app\modules\donors\models\IndicatorFund */
/* @var $fundingDataProvider yii\data\ActiveDataProvider */
/* @var $contributionSearchModel app\modules\donors\models\Contribution */
/* @var $contributionDataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-view">

    <div class="col-md-8">
        <div class="row">
            <div class="row-fluid">
                <div class="span12">
                    <div class="block">
                        <div class="head blue">
                            <div class="icon"><span class="ico-money-bag"></span></div>
                            <h2>Funded Initiatives for <?=$model->name?></h2>
                            <?php if(UserHelper::isAdmin()):?>
                                <span class="pull-right">
                                    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Funding', ['/donors/indicator-fund/create', 'agency' => $model->id], ['class' => 'btn btn-default']) ?>
                                </span>
                            <?php endif?>
                        </div>
                        <div class="data-fluid">
                            <?php $fundingColumns = [
                                ['class' => 'yii\grid\SerialColumn'],

                                ['attribute'=>'indicator_id', 'value'=>'indicator.description'],
                                ['attribute'=>'contribution_amount', 'value'=>'amountWithCurrency', 'label'=>'Funding Amount'],
                                ['attribute'=>'amount_ugx', 'value'=>function(IndicatorFund $data){
                                    return number_format($data->amount_ugx);}
                                ],
                                'fiscal_year',
                                // 'added_on',
                            ];
                            if(UserHelper::isAdmin())
                                $fundingColumns[]=[
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{update} {delete}',
                                    'buttons'=>[
                                        'update'=>function($url, $model){
                                            return Html::a('<i class="glyphicon glyphicon-wrench"></i>',
                                                ['/donors/indicator-fund/update', 'id'=>$model->id],
                                                ['style'=>'color:green','title'=>'Update']
                                            );
                                        },
                                        'delete'=>function($url, $model){
                                            return Html::a('<i class="glyphicon glyphicon-trash"></i>',
                                                ['/donors/indicator-fund/delete', 'id'=>$model->id],
                                                [
                                                    'style'=>'color:red',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this initiative?',
                                                        'method' => 'post',
                                                    ],
                                                ]
                                            );
                                        },
                                    ]
                                ]?>
                            <?= GridView::widget([
                                'dataProvider' => $fundingDataProvider,
                                'summary'=>'',
                                'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                                'rowOptions'=>  function ($model, $key, $index, $grid){
                                    return \app\helper\FormHelper::tableRowOptions();
                                },
                                'columns' => $fundingColumns,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="row-fluid">
                <div class="span12">
                    <div class="block">
                        <div class="head blue">
                            <div class="icon"><span class="ico-envelope-2"></span></div>
                            <h2>Initiative Contributions for <?=$model->name?></h2>
                            <?php if(UserHelper::isAdmin()):?>
                                <span class="pull-right">
                                    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Contribution', ['/donors/contributions/create', 'agency' => $model->id], ['class' => 'btn btn-default']) ?>
                                </span>
                            <?php endif?>
                        </div>
                        <div class="data-fluid">
                            <?php $contributionColumns = [
                                ['class' => 'yii\grid\SerialColumn'],

                                ['attribute'=>'indicator_id', 'value'=>'indicator.description'],
                                ['attribute'=>'name', 'label'=>'Contribution'],
                                ['attribute'=>'description',],
                                // 'added_on',
                                'fiscalYear'
                            ];
                            if(UserHelper::isAdmin())
                                $contributionColumns[] = [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{update} {delete}',
                                    'buttons'=>[
                                        'update'=>function($url, $model){
                                            return Html::a('<i class="glyphicon glyphicon-wrench"></i>',
                                                ['/donors/contributions/update', 'id'=>$model->id],
                                                ['style'=>'color:green','title'=>'Update']
                                            );
                                        },
                                        'delete'=>function($url, $model){
                                            return Html::a('<i class="glyphicon glyphicon-trash"></i>',
                                                ['/donors/contributions/delete', 'id'=>$model->id],
                                                [
                                                    'style'=>'color:red',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this contribution?',
                                                        'method' => 'post',
                                                    ],
                                                ]
                                            );
                                        },
                                    ]
                                ]?>
                            <?= GridView::widget([
                                'dataProvider' => $contributionDataProvider,
                                'summary'=>'',
                                'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                                'rowOptions'=>  function ($model, $key, $index, $grid){
                                    return \app\helper\FormHelper::tableRowOptions();
                                },
                                'columns' => $contributionColumns,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <div class="col-md-4">
        <h3 class="head">
            Detail
            <?= UserHelper::isAdmin()?Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']):'' ?>
            <?= UserHelper::isAdmin()?Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger pull-right',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this partner? Data of their contributions will also be lost.',
                    'method' => 'post',
                ],
            ]):''?>
        </h3>
        <?php $attributes = [
            'name',
            'description',
        ];

        if(UserHelper::isAdmin()){
            $attributes[] = 'contact_person';
            $attributes[] = 'contact_email:email';
            $attributes[] = 'contact_phone';
            //['attribute'=>'is_approved', 'label'=>$model->getAttributeLabel('is_approved'), 'value'=>$model->verboseApproval],
        }

        $attributes[] = 'added_on';

        ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => $attributes,
        ]) ?>
    </div>

</div>
