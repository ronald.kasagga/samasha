<?php

use app\models\Indicator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndicatorFund */
/* @var $form yii\widgets\ActiveForm */
/* @var $indicators array */
/* @var $currencies array */
Indicator::setCurrentYear(date('Y')-1);
$model->fiscal_year = (($model->fiscal_year == null)?Indicator::getCurrentFiscalYear():$model->fiscal_year);
?>

<div class="indicator-fund-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row form-group field-indicatorfund-indicator_id required">
        <label class="control-label  col-lg-12" for="indicatorfund-indicator_id">
            <?=$model->getAttributeLabel('indicator_id')?></label>
        <?=Html::activeDropDownList($model, 'indicator_id', $indicators,
            ['prompt'=>'Choose Indicator...', 'class'=>'select col-lg-12', ])?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'contribution_amount')->textInput() ?>

    <?= $form->field($model, 'currency')->dropDownList($currencies, ['prompt'=>'Choose Currency...']) ?>

    <?= $form->field($model, 'fiscal_year')->dropDownList($fiscal_years) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
