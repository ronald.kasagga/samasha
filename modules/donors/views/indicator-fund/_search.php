<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\donors\models\IndicatorFund */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="indicator-fund-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'indicator_id') ?>

    <?= $form->field($model, 'agency_id') ?>

    <?= $form->field($model, 'contribution_amount') ?>

    <?= $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'amount_ugx') ?>

    <?php // echo $form->field($model, 'added_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
