<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IndicatorFund */
/* @var $indicators array */
/* @var $currencies array */

$this->title = 'Update Indicator Fund: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Indicator Funds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="indicator-fund-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'indicators' => $indicators,
        'currencies' => $currencies,
        'fiscal_years' => $fiscal_years,
    ]) ?>

</div>
