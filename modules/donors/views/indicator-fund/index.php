<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\donors\models\IndicatorFund */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $indicators array */
/* @var $currencies array */

$this->title = 'Indicator Funds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-fund-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Indicator Fund', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'indicator_id',
            'agency_id',
            'contribution_amount',
            'currency',
            // 'amount_ugx',
            // 'added_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
