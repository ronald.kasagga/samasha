<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IndicatorFund */
/* @var $indicators array */
/* @var $currencies array */

$this->title = 'Add Indicator Funding';
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $agency->name, 'url' => ['/donors/donor/view', 'id'=>$agency->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Indicator Funds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-fund-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'indicators' => $indicators,
        'currencies' => $currencies,
        'fiscal_years' => $fiscal_years,
    ]) ?>

</div>
