<?php

namespace app\modules\donors\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contribution as ContributionModel;

/**
 * Contribution represents the model behind the search form about `app\models\Contribution`.
 */
class Contribution extends ContributionModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'indicator_id', 'agency_id'], 'integer'],
            [['name', 'description', 'contributed_on', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContributionModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $this->indicator_id,
            'agency_id' => $this->agency_id,
            'contributed_on' => $this->contributed_on,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function agency($params, $agency_id){
        $query = ContributionModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $this->indicator_id,
            'agency_id' => $agency_id,
            'contributed_on' => $this->contributed_on,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function agencyForIndicator($params, $agency_id, $indicator_id)
    {
        $query = ContributionModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $indicator_id,
            'agency_id' => $agency_id,
            'contributed_on' => $this->contributed_on,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
