<?php

namespace app\modules\donors\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IndicatorFund as IndicatorFundModel;

/**
 * IndicatorFund represents the model behind the search form about `app\models\IndicatorFund`.
 */
class IndicatorFund extends IndicatorFundModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'indicator_id', 'agency_id'], 'integer'],
            [['contribution_amount'], 'number'],
            [['currency', 'amount_ugx', 'added_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IndicatorFundModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $this->indicator_id,
            'agency_id' => $this->agency_id,
            'contribution_amount' => $this->contribution_amount,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'amount_ugx', $this->amount_ugx]);

        return $dataProvider;
    }

    public function agency($params, $agency_id){
        $query = IndicatorFundModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $this->indicator_id,
            'agency_id' => $agency_id,
            'contribution_amount' => $this->contribution_amount,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'amount_ugx', $this->amount_ugx]);

        return $dataProvider;
    }

    public function agencyForIndicator($params, $agency_id, $indicator_id)
    {
        $query = IndicatorFundModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator_id' => $indicator_id,
            'agency_id' => $agency_id,
            'contribution_amount' => $this->contribution_amount,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'amount_ugx', $this->amount_ugx]);

        return $dataProvider;
    }
}
