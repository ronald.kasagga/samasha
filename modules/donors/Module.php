<?php

namespace app\modules\donors;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\donors\controllers';
    public $defaultRoute = 'donor/index';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function getAnchors($controller = null){
        $routes = [
            'donors'=>[
                'Manage'=>['/donors/donor/index'],
                'Create'=>['/donors/donor/create'],
            ]
        ];

        if($controller && isset($routes[$controller]))
            return $routes[$controller];

        return $routes;
    }
}
