<?php

namespace app\modules\donors\controllers;

use app\helper\FormHelper;
use app\models\Currency;
use app\models\Donor;
use app\models\Indicator;
use app\models\IndicatorStatus;
use Yii;
use app\models\IndicatorFund;
use app\modules\donors\models\IndicatorFund as IndicatorFundSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * IndicatorFundController implements the CRUD actions for IndicatorFund model.
 */
class IndicatorFundController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IndicatorFund models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicatorFundSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ], $this->formData()));
    }

    /**
     * Displays a single IndicatorFund model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
        ], $this->formData()));
    }

    /**
     * Creates a new IndicatorFund model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($agency)
    {
        $model = new IndicatorFund();

        if ($model->load(Yii::$app->request->post())) {
            $model->agency_id = $agency;
            $model->fiscal_year = $_POST['IndicatorFund']['fiscal_year'];
            if($model->save())
                return $this->redirect(['/donors/donor/view', 'id' => $agency]);
        }

        return $this->render('create', ArrayHelper::merge([
            'model' => $model,
            'agency' => Donor::findOne($agency),
        ], $this->formData()));
    }

    /**
     * Updates an existing IndicatorFund model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $agency = $model->agency_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->agency_id = $agency;
            if($model->save())
                return $this->redirect(['/donors/donor/view', 'id' => $model->agency_id]);
        }
        return $this->render('update', ArrayHelper::merge([
            'model' => $model,
        ], $this->formData()));
    }

    /**
     * Deletes an existing IndicatorFund model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $agency_id = $model->agency_id;
        $model->delete();

        return $this->redirect(['/donors/donor/view', 'id'=>$agency_id]);
    }

    /**
     * Finds the IndicatorFund model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IndicatorFund the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IndicatorFund::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData(){
        return [
            'indicators' => ArrayHelper::map(Indicator::find()->all(), 'id', 'description'),
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'code', 'description'),
            'fiscal_years'=>FormHelper::toAssociativeArray(IndicatorStatus::getFiscalYearOptions()),
        ];
    }
}
