<?php

namespace app\modules\donors\controllers;

use app\models\Donor;
use app\models\Indicator;
use Yii;
use app\models\Contribution;
use app\modules\donors\models\Contribution as ContributionSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContributionsController implements the CRUD actions for Contribution model.
 */
class ContributionsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contribution models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContributionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contribution model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contribution model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($agency)
    {
        $model = new Contribution();

        if ($model->load(Yii::$app->request->post())) {
            $model->agency_id = $agency;
            if($model->save())
                return $this->redirect(['/donors/donor/view', 'id' => $agency]);
        }

        return $this->render('create', ArrayHelper::merge([
            'model' => $model,
            'agency' => Donor::findOne($agency),
        ], $this->formData()));
    }

    /**
     * Updates an existing Contribution model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $agency = $model->agency_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->agency_id = $agency;
            if($model->save())
                return $this->redirect(['/donors/donor/view', 'id' => $model->agency_id]);
        }
        return $this->render('update', ArrayHelper::merge([
            'model' => $model,
        ], $this->formData()));
    }

    /**
     * Deletes an existing Contribution model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $agency_id = $model->agency_id;
        $model->delete();

        return $this->redirect(['/donors/donor/view', 'id'=>$agency_id]);
    }

    /**
     * Finds the Contribution model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contribution the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contribution::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData(){
        return [
            'indicators' => ArrayHelper::map(Indicator::find()->all(), 'id', 'description'),
        ];
    }
}
