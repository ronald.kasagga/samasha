<?php

namespace app\modules\donors\controllers;

use app\helper\FormHelper;
use app\modules\donors\models\Contribution;
use app\modules\donors\models\IndicatorFund;
use Yii;
use app\models\Donor;
use app\modules\donors\models\Donor as DonorSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DonorController implements the CRUD actions for Donor model.
 */
class DonorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Donor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DonorSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ], $this->formData()));
    }

    /**
     * Displays a single Donor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $fundSearchModel = new IndicatorFund();
        $contributionSearchModel = new Contribution();
        $fundDataProvider = $fundSearchModel->agency(Yii::$app->request->queryParams, $id);
        $contributionDataProvider = $contributionSearchModel->agency(Yii::$app->request->queryParams, $id);

        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
            'fundingSearchModel' => $fundSearchModel,
            'contributionSearchModel' => $contributionSearchModel,
            'fundingDataProvider' => $fundDataProvider,
            'contributionDataProvider' => $contributionDataProvider,
        ], $this->formData()));
    }

    /**
     * Creates a new Donor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Donor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Updates an existing Donor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Deletes an existing Donor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Donor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Donor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function formData(){
        return [
            'approval_statuses'=>FormHelper::toAssociativeArray(Donor::getStatuses())
        ];
    }
}
