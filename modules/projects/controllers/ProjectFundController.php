<?php

namespace app\modules\projects\controllers;

use app\models\Donor;
use app\models\Project;
use Yii;
use app\models\ProjectFund;
use app\modules\projects\models\ProjectFund as ProjectFundSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectFundController implements the CRUD actions for ProjectFund model.
 */
class ProjectFundController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectFund models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectFundSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ], $this->formData()));
    }

    /**
     * Displays a single ProjectFund model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
        ], $this->formData()));
    }

    /**
     * Creates a new ProjectFund model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProjectFund();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Updates an existing ProjectFund model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Deletes an existing ProjectFund model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProjectFund model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectFund the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectFund::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function formData(){
        return [
            'donors'=>ArrayHelper::map(Donor::find()->all(), 'id', 'name'),
            'projects'=>ArrayHelper::map(Project::find()->all(), 'id', 'name'),
        ];
    }
}
