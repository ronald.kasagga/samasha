<?php

namespace app\modules\projects\controllers;

use app\modules\projects\models\ProjectFund;
use Yii;
use app\models\ProjectFundDeposit;
use app\modules\projects\models\ProjectFundDeposit as ProjectFundDepositSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectFundDepositController implements the CRUD actions for ProjectFundDeposit model.
 */
class ProjectFundDepositController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectFundDeposit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectFundDepositSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ], $this->formData()));
    }

    /**
     * Displays a single ProjectFundDeposit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ArrayHelper::merge([
            'model' => $this->findModel($id),
        ], $this->formData()));
    }

    /**
     * Creates a new ProjectFundDeposit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProjectFundDeposit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Updates an existing ProjectFundDeposit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Deletes an existing ProjectFundDeposit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProjectFundDeposit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectFundDeposit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectFundDeposit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function formData(){
        return [
            'funds'=>ArrayHelper::map(ProjectFund::find()->all(), 'id', 'fundDescription'),
        ];
    }
}
