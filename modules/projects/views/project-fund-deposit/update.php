<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFundDeposit */
/* @var $funds array */

$this->title = 'Update Project Fund Deposit: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Project Fund Deposits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-fund-deposit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'funds' => $funds,
    ]) ?>

</div>
