<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\ProjectFundDeposit */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $funds array */

$this->title = 'Project Fund Deposits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-fund-deposit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project Fund Deposit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'fund_id', 'value'=>'fund.fundDescription',
                'filter'=>Html::activeDropDownList($searchModel, 'fund_id', $funds, ['class'=>'form-control', 'prompt'=>'Choose Fund'])
            ],
            'amount',
            'deposited_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
