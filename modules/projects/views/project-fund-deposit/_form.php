<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFundDeposit */
/* @var $form yii\widgets\ActiveForm */
/* @var $funds array */
?>

<div class="project-fund-deposit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fund_id')->dropDownList($funds, ['prompt'=>'Choose Fund']) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'deposited_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
