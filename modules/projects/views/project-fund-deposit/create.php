<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectFundDeposit */
/* @var $funds array */

$this->title = 'Create Project Fund Deposit';
$this->params['breadcrumbs'][] = ['label' => 'Project Fund Deposits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-fund-deposit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'funds' => $funds,
    ]) ?>

</div>
