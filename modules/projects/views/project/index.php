<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\Project */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $policies array */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            ['attribute'=>'policy_id', 'value'=>'policy.name',
                'filter'=>Html::activeDropDownList($searchModel, 'policy_id', $policies, ['class'=>'form-control','prompt'=>'Choose Policy'])
            ],
            'budget_amount',
            'start_date',
            'end_date',
            // 'added_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
