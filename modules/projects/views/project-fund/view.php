<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFund */

$this->title = $model->fundDescription;
$this->params['breadcrumbs'][] = ['label' => 'Project Funds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-fund-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute'=>'project.name', 'label'=>'Project'],
            ['attribute'=>'donor.name', 'label'=>'Donor'],
            'contribution_amount',
            'date_from',
            'date_to',
            'added_on',
        ],
    ]) ?>

</div>
