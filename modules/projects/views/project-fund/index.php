<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\ProjectFund */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projects array */
/* @var $donors array */

$this->title = 'Project Funds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-fund-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project Fund', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'project_id', 'value'=>'project.name',
                'filter'=>Html::activeDropDownList($searchModel, 'project_id', $projects, ['class'=>'form-control', 'prompt'=>'Choose Project'])
            ],
            ['attribute'=>'donor_id', 'value'=>'donor.name',
                'filter'=>Html::activeDropDownList($searchModel, 'donor_id', $donors, ['class'=>'form-control', 'prompt'=>'Choose Donor'])
            ],
            'contribution_amount',
            //'date_from',
            // 'date_to',
            // 'added_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
