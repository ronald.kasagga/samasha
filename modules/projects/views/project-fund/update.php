<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFund */
/* @var $projects array */
/* @var $donors array */

$this->title = 'Update Project Fund: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Project Funds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-fund-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'projects' => $projects,
        'donors' => $donors,
    ]) ?>

</div>
