<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectFund */
/* @var $projects array */
/* @var $donors array */

$this->title = 'Create Project Fund';
$this->params['breadcrumbs'][] = ['label' => 'Project Funds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-fund-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'projects' => $projects,
        'donors' => $donors,
    ]) ?>

</div>
