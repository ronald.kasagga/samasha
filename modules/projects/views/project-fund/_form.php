<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFund */
/* @var $form yii\widgets\ActiveForm */
/* @var $projects array */
/* @var $donors array */
?>

<div class="project-fund-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_id')->dropDownList($projects, ['prompt'=>'Choose Project']) ?>

    <?= $form->field($model, 'donor_id')->dropDownList($donors, ['prompt'=>'Choose Donor'])  ?>

    <?= $form->field($model, 'contribution_amount')->textInput() ?>

    <?= $form->field($model, 'date_from')->textInput() ?>

    <?= $form->field($model, 'date_to')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
