<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PolicyKpi */
/* @var $policies array */

$this->title = 'Create Policy Kpi';
$this->params['breadcrumbs'][] = ['label' => 'Policy Kpis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-kpi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'policies' => $policies,
    ]) ?>

</div>
