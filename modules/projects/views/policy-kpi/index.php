<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\PolicyKpi */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $policies array */

$this->title = 'Policy Kpis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-kpi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Policy Kpi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'policy_id', 'value'=>'policy.name',
                'filter'=>Html::activeDropDownList($searchModel, 'policy_id', $policies, ['class'=>'form-control', 'prompt'=>'Choose Policy'])
            ],
            'description',
            'recorded_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
