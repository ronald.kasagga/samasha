<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\PolicyKpiStatus */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Policy Kpi Statuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-kpi-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Policy Kpi Status', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'policy_kpi_id',
            'description',
            'status',
            'fiscal_year',
            // 'recorded_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
