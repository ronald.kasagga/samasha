<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PolicyKpiStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="policy-kpi-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'policy_kpi_id')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fiscal_year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recorded_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
