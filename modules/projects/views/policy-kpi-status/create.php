<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PolicyKpiStatus */

$this->title = 'Create Policy Kpi Status';
$this->params['breadcrumbs'][] = ['label' => 'Policy Kpi Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-kpi-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
