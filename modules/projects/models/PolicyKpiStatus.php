<?php

namespace app\modules\projects\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PolicyKpiStatus as PolicyKpiStatusModel;

/**
 * PolicyKpiStatus represents the model behind the search form about `app\models\PolicyKpiStatus`.
 */
class PolicyKpiStatus extends PolicyKpiStatusModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'policy_kpi_id'], 'integer'],
            [['description', 'status', 'fiscal_year', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PolicyKpiStatusModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'policy_kpi_id' => $this->policy_kpi_id,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'fiscal_year', $this->fiscal_year]);

        return $dataProvider;
    }
}
