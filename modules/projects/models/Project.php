<?php

namespace app\modules\projects\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project as ProjectModel;

/**
 * Project represents the model behind the search form about `app\models\Project`.
 */
class Project extends ProjectModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'policy_id'], 'integer'],
            [['name'], 'string'],
            [['budget_amount'], 'number'],
            [['start_date', 'end_date', 'added_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'policy_id' => $this->policy_id,
            'name' => $this->name,
            'budget_amount' => $this->budget_amount,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'added_on' => $this->added_on,
        ]);

        return $dataProvider;
    }
}
