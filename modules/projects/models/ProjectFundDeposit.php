<?php

namespace app\modules\projects\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProjectFundDeposit as ProjectFundDepositModel;

/**
 * ProjectFundDeposit represents the model behind the search form about `app\models\ProjectFundDeposit`.
 */
class ProjectFundDeposit extends ProjectFundDepositModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fund_id'], 'integer'],
            [['amount'], 'number'],
            [['deposited_on', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectFundDepositModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fund_id' => $this->fund_id,
            'amount' => $this->amount,
            'deposited_on' => $this->deposited_on,
            'recorded_on' => $this->recorded_on,
        ]);

        return $dataProvider;
    }
}
