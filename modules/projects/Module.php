<?php

namespace app\modules\projects;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\projects\controllers';
    public $defaultRoute = 'policy/index';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function getAnchors($controller = null){
        $routes = [
            'projects'=>[
                //'Policy'=>['/projects/policy/index'],
                'Policy KPIs'=>['/projects/policy-kpi/index'],
                'Policy KPI Status'=>['/projects/policy-kpi-status/index'],
                'Project'=>['/projects/project/index'],
                'Project Fund'=>['/projects/project-fund/index'],
                'Fund Deposit'=>['/projects/project-fund-deposit/index'],
            ]
        ];

        if($controller && isset($routes[$controller]))
            return $routes[$controller];

        return $routes;
    }
}
