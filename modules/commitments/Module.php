<?php

namespace app\modules\commitments;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\commitments\controllers';
    public $defaultRoute = 'policy-statement/index';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function getAnchors($controller = null){
        $routes = [
            'commitments'=>[
                'Policy Statements'=>['/commitments/policy-statement/index'],
                'Implementation Levels'=>['/commitments/implementation-level/index'],
                'Performance Indicators'=>['/commitments/performance-indicator/index'],
            ]
        ];

        if($controller && isset($routes[$controller]))
            return $routes[$controller];

        return $routes;
    }
}
