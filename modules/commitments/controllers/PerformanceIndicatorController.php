<?php

namespace app\modules\commitments\controllers;

use app\helper\FormHelper;
use Yii;
use app\models\PerformanceIndicator;
use app\modules\commitments\models\PerformanceIndicator as PerformanceIndicatorSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PerformanceIndicatorController implements the CRUD actions for PerformanceIndicator model.
 */
class PerformanceIndicatorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PerformanceIndicator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PerformanceIndicatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PerformanceIndicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PerformanceIndicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $level
     * @return mixed
     */
    public function actionCreate($level = null)
    {
        $model = new PerformanceIndicator();
        $model->implementation_level_id = $level;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Updates an existing PerformanceIndicator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ArrayHelper::merge([
                'model' => $model,
            ], $this->formData()));
        }
    }

    /**
     * Deletes an existing PerformanceIndicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PerformanceIndicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PerformanceIndicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerformanceIndicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function formData(){
        return [
            'statuses'=>FormHelper::toAssociativeArray(PerformanceIndicator::getStatuses()),
            'time_frame_types'=>FormHelper::toAssociativeArray(PerformanceIndicator::getTimeFrameTypes())
        ];
    }
}
