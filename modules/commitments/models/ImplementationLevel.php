<?php

namespace app\modules\commitments\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ImplementationLevel as ImplementationLevelModel;

/**
 * ImplementationLevel represents the model behind the search form about `app\models\ImplementationLevel`.
 */
class ImplementationLevel extends ImplementationLevelModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'policy_statement_id'], 'integer'],
            [['description', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImplementationLevelModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'policy_statement_id' => $this->policy_statement_id,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
