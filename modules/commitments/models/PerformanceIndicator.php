<?php

namespace app\modules\commitments\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PerformanceIndicator as PerformanceIndicatorModel;

/**
 * PerformanceIndicator represents the model behind the search form about `app\models\PerformanceIndicator`.
 */
class PerformanceIndicator extends PerformanceIndicatorModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'implementation_level_id'], 'integer'],
            [['description', 'time_from', 'time_to', 'timeframe_type', 'status', 'status_description', 'coalition_action', 'recorded_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerformanceIndicatorModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'implementation_level_id' => $this->implementation_level_id,
            'recorded_on' => $this->recorded_on,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'time_from', $this->time_from])
            ->andFilterWhere(['like', 'time_to', $this->time_to])
            ->andFilterWhere(['like', 'timeframe_type', $this->timeframe_type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status_description', $this->status_description])
            ->andFilterWhere(['like', 'coalition_action', $this->coalition_action]);

        return $dataProvider;
    }
}
