<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PerformanceIndicator */
/* @var $statuses array */

$this->title = 'Create Performance Indicator';
$this->params['breadcrumbs'][] = ['label' => 'Performance Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="performance-indicator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses'=>$statuses,
        'time_frame_types'=>$time_frame_types,
    ]) ?>

</div>
