<?php

use app\models\PerformanceIndicator;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\commitments\models\PerformanceIndicator */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Performance Indicators';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="performance-indicator-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Performance Indicator', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'implementation_level_id', 'value'=>'shortImplementationLevel'],
            'description',
            'time_from',
            'time_to',
            // 'timeframe_type',
            // 'status',
            // 'status_description',
            // 'coalition_action',
            // 'recorded_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
