<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PerformanceIndicator */

$this->title = 'Update Performance Indicator: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Performance Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="performance-indicator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses'=>$statuses,
        'time_frame_types'=>$time_frame_types,
    ]) ?>

</div>
