<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\commitments\models\PerformanceIndicator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="performance-indicator-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'implementation_level_id') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'time_from') ?>

    <?= $form->field($model, 'time_to') ?>

    <?php // echo $form->field($model, 'timeframe_type') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'status_description') ?>

    <?php // echo $form->field($model, 'coalition_action') ?>

    <?php // echo $form->field($model, 'recorded_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
