<?php

use app\models\ImplementationLevel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PerformanceIndicator */
/* @var $form yii\widgets\ActiveForm */
/* @var $statuses array */
?>

<div class="performance-indicator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(isset($model->implementation_level_id))
        echo '<strong>Implementation Level</strong>: '.ImplementationLevel::findOne($model->implementation_level_id)->description?>

    <?= $form->field($model, 'implementation_level_id')->textInput(['type'=>'hidden'])->label(false) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time_from')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time_to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'timeframe_type')->dropDownList($time_frame_types, ['prompt'=>"Choose Timeframe Type..."]) ?>

    <?= $form->field($model, 'status')->dropDownList($statuses, ['prompt'=>'Choose Status...']) ?>

    <?= $form->field($model, 'status_description')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
