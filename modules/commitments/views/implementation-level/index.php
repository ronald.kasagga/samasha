<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\commitments\models\ImplementationLevel */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $policy_statements array */

$this->title = 'Implementation Levels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="implementation-level-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Implementation Level', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'policy_statement_id', 'value'=>'policyStatement.initiative',
                'filter'=>Html::activeDropDownList($searchModel, 'policy_statement_id', $policy_statements, ['class'=>'form-control', 'prompt'=>'Select Initiative'])
            ],
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
