<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImplementationLevel */
/* @var $policy_statements array */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Implementation Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="implementation-level-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Performance Indicator', ['/commitments/performance-indicator/create', 'level' => $model->id],
            ['class' => 'btn btn-info']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute'=>'policy_statement_id', 'value'=>$model->policyStatement->description],
            'description',
            'recorded_on',
        ],
    ]) ?>

</div>
