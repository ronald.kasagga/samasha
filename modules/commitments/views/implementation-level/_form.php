<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImplementationLevel */
/* @var $form yii\widgets\ActiveForm */
/* @var $policy_statements array */
?>

<div class="implementation-level-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'policy_statement_id')->dropDownList($policy_statements, ['prompt' =>'Choose Initiative']) ?>

    <?= $form->field($model, 'description')->textarea(['rows'=>3]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
