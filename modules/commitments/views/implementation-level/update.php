<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImplementationLevel */
/* @var $policy_statements array */

$this->title = 'Update Implementation Level: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Implementation Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="implementation-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'policy_statements' => $policy_statements,
    ]) ?>

</div>
