<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImplementationLevel */
/* @var $policy_statements array */

$this->title = 'Create Implementation Level';
$this->params['breadcrumbs'][] = ['label' => 'Implementation Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="implementation-level-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'policy_statements' => $policy_statements,
    ]) ?>

</div>
