<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Initiative */

$this->title = 'Create Initiative';
//$this->params['breadcrumbs'][] = ['label'=>'Commitments', 'url'=>['/commitments']];
$this->params['breadcrumbs'][] = ['label' => 'Initiatives', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="initiative-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
