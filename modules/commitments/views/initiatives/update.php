<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Initiative */

$this->title = 'Update Initiative: ' . ' ' . $model->name;
//$this->params['breadcrumbs'][] = ['label'=>'Commitments', 'url'=>['/commitments']];
$this->params['breadcrumbs'][] = ['label' => 'Initiatives', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="initiative-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
