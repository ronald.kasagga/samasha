<?php

use app\helper\UserHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\commitments\models\Initiative */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Initiatives';
//$this->params['breadcrumbs'][] = ['label'=>'Commitments', 'url'=>['/commitments']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="initiative-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $columns = [
        ['class' => 'yii\grid\SerialColumn'],

        ['attribute'=>'name', 'format'=>'raw', 'value'=>function(\app\models\Initiative $data){
            return isset($data->link)?Html::a($data->name, $data->link, ['target'=>'_blank']):$data->name;
        }],
        'added_on',
    ];

    if(UserHelper::isAdmin()) $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width:140px'],
        'template'=>'{view} {update} {delete}',
        'buttons'=>[
            'view'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-eye-open"></span></div>', $url, ['class'=>'button green']);
            },
            'update'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-pencil-2"></span></div>', $url, ['class'=>'button blue']);
            },
            'delete'=>function($url, $model){
                return Html::a('<div class="icon"><span class="ico-trash"></span></div>', $url, ['class'=>'button red']);
            },
        ]
    ]?>
    <div class="row-fluid">
        <div class="span12">
            <div class="block">
                <div class="head blue">
                    <div class="icon"><span class="ico-open"></span></div>
                    <h2><?=$this->title?></h2>
                    <?php if(UserHelper::isAdmin()):?>
                        <span class="pull-right">
                            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Create Initiative', ['create'], ['class' => 'btn']) ?>
                        </span>
                    <?php endif?>
                </div>
                <div class="data-fluid">
                    <?= GridView::widget(array(
                        'dataProvider' => $dataProvider,
                        'summary'=>'',
                        'tableOptions'=>\app\helper\FormHelper::tableOptions(),
                        'rowOptions'=>  function ($model, $key, $index, $grid){
                            return \app\helper\FormHelper::tableRowOptions();
                        },
                        'columns' => $columns
                    )); ?>
                </div>
            </div>
        </div>
</div>
