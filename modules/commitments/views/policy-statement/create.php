<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PolicyStatement */

$this->title = 'Create Policy Statement';
$this->params['breadcrumbs'][] = ['label' => 'Policy Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-statement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
