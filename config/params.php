<?php

return [
    'adminEmail' => 'admin@example.com',
    'app-vendor' => 'RythTech',
    'app-vendor-website' => 'rythtech.com',
    'app-vendor-email' => 'info@rythtech.com',
];
