<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_204515_correct_indicator_theme_fk extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_indicator_theme_id', '{{%indicator}}');
        $this->addForeignKey('fk_indicator_theme_id', '{{%indicator}}', 'theme_id', '{{%policy}}', 'id', 'CASCADE', 'RESTRICT');

    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_indicator_theme_id', '{{%indicator}}');
        $this->addForeignKey('fk_indicator_theme_id', '{{%indicator}}', 'theme_id', '{{%project}}', 'id', 'CASCADE', 'RESTRICT');
    }
}
