<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_130112_log_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id'=>Schema::TYPE_PK,
            'object_category'=>Schema::TYPE_STRING . ' NOT NULL',
            'object_id'=>Schema::TYPE_STRING . ' NOT NULL',
            'user_id'=>Schema::TYPE_INTEGER,
            'status'=>Schema::TYPE_STRING . ' NOT NULL',
            'message'=>Schema::TYPE_STRING . ' NOT NULL',
            'detail'=>Schema::TYPE_TEXT,
            'recorded_on'=>Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
