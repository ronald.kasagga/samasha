<?php

use yii\db\Schema;
use yii\db\Migration;

class m150726_201231_add_name_to_project extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%project}}', 'name', Schema::TYPE_STRING. ' NOT NULL');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%project}}', 'name');
    }
}
