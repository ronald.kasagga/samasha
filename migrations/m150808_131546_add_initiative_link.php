<?php

use yii\db\Schema;
use yii\db\Migration;

class m150808_131546_add_initiative_link extends Migration
{
    public function safeUp()
    {
        $this->addColumn('initiative', 'link', Schema::TYPE_TEXT);
    }
    
    public function safeDown()
    {
        $this->dropColumn('initiative', 'link');
    }
}
