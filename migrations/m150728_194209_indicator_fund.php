<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_194209_indicator_fund extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%indicator_fund}}', [
            'id'=>Schema::TYPE_PK,
            'indicator_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'agency_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'contribution_amount'=>Schema::TYPE_DOUBLE . ' NOT NULL',
            'currency'=>Schema::TYPE_STRING . ' NOT NULL',
            'amount_ugx'=>Schema::TYPE_STRING . ' NOT NULL',
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_indicator_funding', '{{%indicator_fund}}', 'indicator_id', '{{%indicator}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_indicator_fund_donor', '{{%indicator_fund}}', 'agency_id', '{{%donor}}', 'id', 'CASCADE', 'RESTRICT');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%indicator_fund}}');
    }
}
