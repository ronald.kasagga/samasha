<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_200954_adjust_indicator_add_initiative extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%initiative}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->dropColumn('{{%indicator}}', 'theme');
        $this->addColumn('{{%indicator}}', 'theme_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn('{{%indicator}}', 'initiative_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn('{{%indicator}}', 'name', Schema::TYPE_STRING . ' NOT NULL');

        $this->addForeignKey('fk_indicator_theme_id', '{{%indicator}}', 'theme_id', '{{%project}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_indicator_initiative_id', '{{%indicator}}', 'initiative_id', '{{%initiative}}', 'id', 'CASCADE', 'RESTRICT');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%initiative}}');
        $this->addColumn('{{%indicator}}', 'theme', Schema::TYPE_STRING);
        $this->dropColumn('{{%indicator}}', 'theme_id');
        $this->dropColumn('{{%indicator}}', 'initiative_id');
        $this->dropColumn('{{%indicator}}', 'name');

        $this->dropForeignKey('fk_indicator_initiative_id', '{{%indicator}}');
        $this->dropForeignKey('fk_indicator_theme_id', '{{%indicator}}');
    }
}
