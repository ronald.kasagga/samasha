<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_002319_indicator_status extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%indicator_status}}', [
            'id'=>Schema::TYPE_PK,
            'indicator_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'status'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'fiscal_year'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_indicator_id', '{{%indicator_status}}', 'indicator_id', '{{%indicator}}', 'id', 'CASCADE', 'RESTRICT');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%indicator_status}}');
    }
}
