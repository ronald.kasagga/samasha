<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_081655_delete_2015_2016_indicator_statuses extends Migration
{
    public function safeUp()
    {
        $this->delete('{{%indicator_status}}', [
           'fiscal_year'=>'2015/2016'
        ]);
    }
    
    public function safeDown()
    {
        echo "m160211_081655_delete_2015_2016_indicator_statuses cannot be reverted.\n";
        return false;
    }
}
