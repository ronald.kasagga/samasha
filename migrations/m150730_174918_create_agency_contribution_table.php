<?php

use yii\db\Schema;
use yii\db\Migration;

class m150730_174918_create_agency_contribution_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%contribution}}', [
            'id'=>Schema::TYPE_PK,
            'indicator_id'=>Schema::TYPE_INTEGER .' NOT NULL',
            'agency_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_TEXT ,
            'contributed_on'=>Schema::TYPE_DATETIME ,
            'recorded_on'=>Schema::TYPE_TIMESTAMP .' NOT NULL' ,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%contribution}}');
    }
}
