<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_235833_comodities extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%commodity}}', [
            'id'=>Schema::TYPE_PK,
            'indicator_id'=>Schema::TYPE_INTEGER .' NOT NULL',
            'agency_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_TEXT ,
            'expenditure'=>Schema::TYPE_DOUBLE . ' NOT NULL',
            'currency'=>Schema::TYPE_TEXT .' NOT NULL',
            'expenditure_ugx'=>Schema::TYPE_DOUBLE . ' NOT NULL',
            'deposited_on'=>Schema::TYPE_DATETIME ,
            'recorded_on'=>Schema::TYPE_TIMESTAMP .' NOT NULL' ,
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%commodity}}');
    }
}
