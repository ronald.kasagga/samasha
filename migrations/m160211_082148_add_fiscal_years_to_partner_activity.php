<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_082148_add_fiscal_years_to_partner_activity extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%indicator_fund}}', 'fiscal_year', Schema::TYPE_STRING);
        $this->addColumn('{{%commodity}}', 'fiscal_year', Schema::TYPE_STRING);
        $this->update('{{%indicator_fund}}', ['fiscal_year'=>'2014/2015']);
        $this->update('{{%commodity}}', ['fiscal_year'=>'2014/2015']);
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%indicator_fund}}', 'fiscal_year');
        $this->dropColumn('{{%commodity}}', 'fiscal_year');
    }
}
