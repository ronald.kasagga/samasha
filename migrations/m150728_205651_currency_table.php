<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_205651_currency_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id'=>Schema::TYPE_PK,
            'code'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_TEXT . ' NOT NULL',
            'ugx_exchange'=>Schema::TYPE_STRING. ' NOT NULL',
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
