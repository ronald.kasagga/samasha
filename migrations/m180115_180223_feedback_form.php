<?php

use yii\db\Migration;

/**
 * Class m180115_180223_feedback_form
 */
class m180115_180223_feedback_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id'=>$this->primaryKey(),
            'email'=>$this->string()->notNull(),
            'name'=>$this->string()->null(),
            'contact'=>$this->string()->null(),
            'message'=>$this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
