<?php

use app\models\IndicatorStatus;
use yii\db\Migration;

class m150907_065232_update_status_strings extends Migration
{
    public function safeUp()
    {
        $this->update('{{%indicator_status}}', [
            'status'=>IndicatorStatus::STATUS_ACHIEVED
        ], "status = 'on_track'");
        $this->update('{{%indicator_status}}', [
            'status'=>IndicatorStatus::STATUS_ON_TRACK
        ], "status = 'delayed'");
        $this->update('{{%indicator_status}}', [
            'status'=>IndicatorStatus::STATUS_NOT_ACHIEVED
        ], "status = 'not_started'");
    }
    
    public function safeDown()
    {
        $achieved = IndicatorStatus::STATUS_ACHIEVED;
        $on_track = IndicatorStatus::STATUS_ON_TRACK;
        $not_achieved = IndicatorStatus::STATUS_NOT_ACHIEVED;

        $this->update('{{%indicator_status}}', [
            'status'=>'delayed'
        ], "status = '$on_track'");
        $this->update('{{%indicator_status}}', [
            'status'=>'on_track'
        ], "status = '$achieved'");
        $this->update('{{%indicator_status}}', [
            'status'=>'not_started'
        ], "status = '$not_achieved'");
    }
}
