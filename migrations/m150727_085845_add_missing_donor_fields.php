<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_085845_add_missing_donor_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%donor}}', 'description', Schema::TYPE_STRING);
        $this->addColumn('{{%donor}}', 'contact_person', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('{{%donor}}', 'contact_email', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('{{%donor}}', 'contact_phone', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('{{%donor}}', 'is_approved', Schema::TYPE_STRING.' NOT NULL');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%donor}}', 'description');
        $this->dropColumn('{{%donor}}', 'contact_person');
        $this->dropColumn('{{%donor}}', 'contact_email');
        $this->dropColumn('{{%donor}}', 'contact_phone');
        $this->dropColumn('{{%donor}}', 'is_approved');
    }
}
