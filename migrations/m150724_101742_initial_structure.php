<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_101742_initial_structure extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%policy}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->createTable('{{%project}}', [
            'id'=>Schema::TYPE_PK,
            'policy_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'budget_amount' => Schema::TYPE_DOUBLE . ' NOT NULL',
            'start_date' => Schema::TYPE_DATETIME . ' NOT NULL',
            'end_date' => Schema::TYPE_DATETIME . ' NOT NULL',
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_policy_project', '{{%project}}', 'policy_id', '{{%policy}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%donor}}', [
            'id'=>Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('donor_unique_name', '{{%donor}}', 'name', true);

        $this->createTable('{{%project_fund}}', [
            'id'=>Schema::TYPE_PK,
            'project_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'donor_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'contribution_amount'=>Schema::TYPE_DOUBLE . ' NOT NULL',
            'date_from'=>Schema::TYPE_DATETIME ,
            'date_to'=>Schema::TYPE_DATETIME ,
            'added_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_project_funding', '{{%project_fund}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_project_fund_donor', '{{%project_fund}}', 'donor_id', '{{%donor}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%project_fund_deposit}}', [
            'id'=>Schema::TYPE_PK,
            'fund_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'amount'=>Schema::TYPE_DOUBLE . ' NOT NULL',
            'deposited_on'=>Schema::TYPE_DATETIME . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_project_fund_deposit', '{{%project_fund_deposit}}', 'fund_id', '{{%project_fund}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%indicator}}', [
            'id'=>Schema::TYPE_PK,
            'theme'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->createTable('{{%policy_kpi}}', [
            'id'=>Schema::TYPE_PK,
            'policy_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_policy_kpi', '{{%policy_kpi}}', 'policy_id', '{{%policy}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%policy_kpi_status}}', [
            'id'=>Schema::TYPE_PK,
            'policy_kpi_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'status'=>Schema::TYPE_STRING . ' NOT NULL',
            'fiscal_year'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_policy_kpi_status', '{{%policy_kpi_status}}', 'policy_kpi_id', '{{%policy_kpi}}', 'id', 'CASCADE', 'RESTRICT');

        //commitment zone
        $this->createTable('{{%policy_statement}}', [
            'id'=>Schema::TYPE_PK,
            'initiative'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->createTable('{{%implementation_level}}', [
            'id'=>Schema::TYPE_PK,
            'policy_statement_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_policy_statement_id', '{{%implementation_level}}', 'policy_statement_id', '{{%policy_statement}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%performance_indicator}}', [
            'id'=>Schema::TYPE_PK,
            'implementation_level_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'description'=>Schema::TYPE_STRING . ' NOT NULL',
            'time_from'=>Schema::TYPE_STRING ,
            'time_to'=>Schema::TYPE_STRING ,
            'timeframe_type'=>Schema::TYPE_STRING ,
            'status'=>Schema::TYPE_STRING . ' NOT NULL',
            'status_description'=>Schema::TYPE_STRING ,
            'coalition_action'=>Schema::TYPE_STRING ,
            'recorded_on' => Schema::TYPE_TIMESTAMP .' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey('fk_performance_indicator', '{{%performance_indicator}}', 'implementation_level_id', '{{%performance_indicator}}', 'id', 'CASCADE', 'RESTRICT');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%performance_indicator}}');
        $this->dropTable('{{%implementation_level}}');
        $this->dropTable('{{%policy_statement}}');
        $this->dropTable('{{%policy_kpi_status}}');
        $this->dropTable('{{%policy_kpi}}');
        $this->dropTable('{{%indicator}}');
        $this->dropTable('{{%project_fund_deposit}}');
        $this->dropTable('{{%project_fund}}');
        $this->dropTable('{{%donor}}');
        $this->dropTable('{{%project}}');
        $this->dropTable('{{%policy}}');
    }
}
