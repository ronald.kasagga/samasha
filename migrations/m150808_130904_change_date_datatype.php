<?php

use yii\db\Schema;
use yii\db\Migration;

class m150808_130904_change_date_datatype extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('contribution', 'contributed_on', Schema::TYPE_DATE);
    }
    
    public function safeDown()
    {
        $this->alterColumn('contribution', 'contributed_on', Schema::TYPE_DATETIME);
    }

}
