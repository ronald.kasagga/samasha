<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_082040_remove_donor_description_limit extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%donor}}', 'description', Schema::TYPE_TEXT);
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%donor}}', 'description', Schema::TYPE_STRING);
    }
}
