<?php
/**
 * Created by PhpStorm.
 * User: sagas
 * Date: 15/01/18
 * Time: 21:38
 */

namespace app\helper;


use yii\db\ActiveRecord;
use yii\db\Exception;

class ExportHelper
{
    public static function exportExcel($class, $filename, $ext){
        /*$size = 0;
        try {
            $size = \Yii::$app->db
                ->createCommand("SELECT data_length FROM information_schema.TABLES WHERE TABLE_NAME = '{$model->tableName()}'")
                ->queryScalar();
        } catch (Exception $e) {
        }
        header("Content-Type: application/$ext");
        header("Content-Disposition: attachment; filename=$filename.$ext");
        header("Pragma: no-cache");
        header("Content-Length: $size");
        header("Expires: 0");*/

        $content = join("\t", (new $class)->attributeLabels()) . "\n";
        $i = ['limit'=>20, 'offset'=>0, 'total'=>$class::find()->count()];
        for($i['offset'] = 0; $i['offset'] < $i['total']; $i['offset'] += $i['limit']){
            foreach ($class::find()->limit($i['limit'])->offset($i['offset'])->asArray() as $row) var_dump($row);
                //$content .= join("\t", $row) . "\n";
        }
        \Yii::$app->response->sendContentAsFile($content, "$filename.$ext");
        //return $content;
    }
}