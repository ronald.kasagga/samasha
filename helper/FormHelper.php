<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/27/15
 * Time: 12:31 PM
 */

namespace app\helper;

use app\models\Indicator;
use yii\helpers\Html;

class FormHelper
{
    const STATUS_SUCCESS = 'success';
    const STATUS_PENDING = 'warning';
    const STATUS_FAILED = 'danger';

    public static function toAssociativeArray(array $dataArray, $convertKeys = false){
        $result = [];

        if(!empty($dataArray))
            foreach($dataArray as $option)
                $result[$convertKeys?self::toMachineFriendlyString($option):$option] = self::toUserFriendlyString($option);

        return $result;
    }

    public static function toMachineFriendlyString($string)
    {
        return strtolower(preg_replace('/[\s+,-.;:]/', '_', $string));
    }

    private static function toUserFriendlyString($string)
    {
        $result = self::expressString($string);
        return ($result != '') ? $result : $string;
    }

    public static function statusBadge($status, $info)
    {
        return "<span class=' label label-$status'>$info</span>";
    }

    public static function expressString($string)
    {
        return trim(ucwords(preg_replace('/[.,-_;:]/', ' ', $string)));
    }

    public static function tableRowOptions()
    {
        return ['style'=>'height:20px!important'];
    }

    public static function tableOptions()
    {
        return ['cellspacing'=>0, 'cellpadding'=>0, 'class'=>'table'];
    }

    public static function fiscalYearForm()
    {
        $fiscalYear = Indicator::getCurrentFiscalYear();

        list($lastYear, $thisYear) = explode('/',$fiscalYear);
        $form_id = uniqid('fiscal-yr-');
        $form = Html::beginForm('', 'post', ['class'=>'form-inline', 'id'=>$form_id]);
        $form .= "<div class='form-group pull-right'>";
        $form .= Html::dropDownList(
            Indicator::FISCAL_YEAR_POST_NAME,
            $lastYear,
            Indicator::fiscalYears(),
            ['onchange'=>"document.getElementById('$form_id').submit()", 'style'=>'border:none; margin-top: -4px']
        );
        $form .= "</div>";
        $form .= Html::endForm();
        return $form;
    }
}