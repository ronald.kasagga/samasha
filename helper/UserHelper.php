<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/4/15
 * Time: 6:14 PM
 */

namespace app\helper;


use dektrium\user\models\User;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class UserHelper
{
    private static $_adminUsers;

    public static function isAdmin(){
        if(Yii::$app->user->isGuest)
            return false;

        $userIsAdminKey = 'user.isAdmin';

        if(!Yii::$app->session->has($userIsAdminKey)){
            $admins = self::getAdminUserArray();
            $isAdmin = in_array(Yii::$app->user->identity->username, $admins);
            Yii::$app->session->set($userIsAdminKey, $isAdmin);
        }

        return Yii::$app->session->get($userIsAdminKey);
    }

    public static function isUserAdmin(User $user){
        $admins = self::getAdminUserArray();
        return in_array($user->username, $admins);
    }

    public static function getUserPicUrl()
    {
        return Url::base().'/img/examples/users/dmitry_m.jpg';
    }

    public static function getUserFullName(User $user){
        return (isset($user->profile) && !in_array($user->profile->name, ['', null])) ? $user->profile->name : $user->username;
    }

    public static function adminStatusButton(User $model)
    {
        $userIsAdmin = self::isUserAdmin($model);
        return Html::a(
            ($userIsAdmin?'<i class="glyphicon glyphicon-minus"></i>':'<i class="glyphicon glyphicon-plus"></i>').' Admin',
            ['/site/'.($userIsAdmin?'revoke':'grant').'-user', 'username'=>$model->username],
            ['class'=>'btn btn-'.($userIsAdmin?'danger':'success'), 'title'=>($userIsAdmin?'Revoke':'Grant').' Admin Rights']);
    }

    private static function getAdminUserArray()
    {
        if(empty(self::$_adminUsers))
            self::$_adminUsers = require(self::getAdminFileLink());

        return self::$_adminUsers;
    }

    public static function addToAdmin($username){
        $admins = self::getAdminUserArray();
        if(!in_array($username, $admins)){
            $admins[] = $username;
            return self::writeAdminFile($admins);
        }

        return false;
    }

    public static function removeFromAdmin($username){
        $admins = self::getAdminUserArray();
        if(in_array($username, $admins)){
            $newAdmins = [];
            foreach($admins as $user)
                if($user != $username)
                    $newAdmins[] = $user;
            return self::writeAdminFile($newAdmins);
        }
        return false;
    }

    private static function writeAdminFile($admins)
    {
        $adminFileLink = self::getAdminFileLink();
        $data = "<?php\n return[\n\t '" . implode("','", $admins)."'\n ];";
        return file_put_contents($adminFileLink, $data);
    }

    private static function getAdminFileLink()
    {
        return Yii::getAlias('@app/config/admins.php');
    }
}