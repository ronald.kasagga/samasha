<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 7:24 PM
 */

namespace app\helper;


use dektrium\user\models\Profile;
use Yii;
use yii\bootstrap\Nav;

class NavHelper
{
    public static function generateSingleNavLevel($items, $class = 'sub'){
        $itemOptions = [];

        foreach($items as $label => $url)
            $itemOptions[] = ['label'=>$label, 'url'=>$url, 'active'=>('/'.\Yii::$app->controller->route == $url[0])];

        return Nav::widget([
            'options'=>['class'=>$class],
            'items'=>$itemOptions
        ]);
    }

    public static function isModuleActive($module)
    {
        return (isset(\Yii::$app->controller->module) && (\Yii::$app->controller->module->id == $module));
    }

    public static function isActiveClass($module)
    {
        return self::isModuleActive($module) ? 'active' : '';
    }

    public static function isActiveController($controller)
    {
        return (\Yii::$app->controller->id == $controller) ? 'active' : '';
    }

    public static function isHome()
    {
        return \Yii::$app->controller->route == 'site/index';
    }

    public static function getUserDisplayName(){
        $null = ['', null];
        $username_key ='user.fullName';

        if(!Yii::$app->session->has($username_key))
            if(($profile=Profile::findOne(Yii::$app->user->id)) != null)
                Yii::$app->session->set($username_key, !in_array($profile->name, $null) ? $profile->name : null);

        if(!in_array(Yii::$app->session->get($username_key), $null))
            return Yii::$app->session->get($username_key);

        return Yii::$app->user->identity->username;
    }
}