<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 9/2/2015
 * Time: 12:21 PM
 */

namespace app\helper;


class TimeHelper
{
    public static function formatDate($date, $format = 'F d, Y')
    {
        return date($format, strtotime($date));
    }

    public static function greet(){
        $hour = date("H");
        if($hour < 12 ) return 'Good Morning';
        if($hour < 16 ) return 'Good Afternoon';
        return 'Good Evening';
    }
}