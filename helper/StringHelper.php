<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 1/18/2016
 * Time: 1:22 PM
 */

namespace app\helper;


class StringHelper
{
    public static function formatStringWithBreaks($string){
        $phrase = self::prependWhereExists([
            '(i)', '(ii)', '(iii)', '(iv)', '(v)', ], "\n", $string);
        $phrase = str_replace(["\r\n", "\n\r", "\r", "\n"], '<br>', $phrase);
        return $phrase;
    }

    private static function prependWhereExists($terms, $prepend, $phrase)
    {
        if(!empty($terms))
            foreach ($terms as $term)
                $phrase = str_replace($term, $prepend.$term, $phrase);

        return $phrase;
    }
}